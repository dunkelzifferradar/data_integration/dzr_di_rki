CREATE TABLE RAW_RKI (
	INSERT_DATE TIMESTAMP,
	IDBUNDESLAND INTEGER,
	BUNDESLAND VARCHAR(32),
	LANDKREIS VARCHAR(128),
	ALTERSGRUPPE VARCHAR(32),
	GESCHLECHT VARCHAR(16),
	ANZAHLFALL INTEGER,
	ANZAHLTODESFALL INTEGER,
	OBJECTID INTEGER,
	MELDEDATUM VARCHAR(32),
	IDLANDKREIS INTEGER,
	DATENSTAND VARCHAR(32),
	NEUERFALL INTEGER,
	NEUERTODESFALL INTEGER,
	REFDATUM VARCHAR(32),
	NEUGENESEN INTEGER,
	ANZAHLGENESEN INTEGER,
	ISTERKRANKUNGSBEGINN INTEGER,
	ALTERSGRUPPE2 VARCHAR(32),
	PRIMARY KEY (OBJECTID)
);

create table SOR_DIM_COUNTRY (
	ID smallint,
	NAME varchar(64),
	POPULATION integer,
	PRIMARY KEY (ID),
	UNIQUE (NAME)
);
insert into sor_dim_country values (0, 'GERMANY', 83019213);

create table STG_DIM_STATE (
	ID smallint,
	COUNTRY_ID smallint,
	NAME varchar(32),
	PRIMARY KEY (ID),
	FOREIGN KEY (COUNTRY_ID) REFERENCES SOR_DIM_COUNTRY (ID),
	UNIQUE(COUNTRY_ID, NAME)
);

INSERT INTO stg_dim_state VALUES (2, 0, 'Hamburg');
INSERT INTO stg_dim_state VALUES (3, 0, 'Niedersachsen');
INSERT INTO stg_dim_state VALUES (15, 0, 'Sachsen-Anhalt');
INSERT INTO stg_dim_state VALUES (8, 0, 'Baden-Württemberg');
INSERT INTO stg_dim_state VALUES (1, 0, 'Schleswig-Holstein');
INSERT INTO stg_dim_state VALUES (5, 0, 'Nordrhein-Westfalen');
INSERT INTO stg_dim_state VALUES (10, 0, 'Saarland');
INSERT INTO stg_dim_state VALUES (7, 0, 'Rheinland-Pfalz');
INSERT INTO stg_dim_state VALUES (13, 0, 'Mecklenburg-Vorpommern');
INSERT INTO stg_dim_state VALUES (9, 0, 'Bayern');
INSERT INTO stg_dim_state VALUES (11, 0, 'Berlin');
INSERT INTO stg_dim_state VALUES (14, 0, 'Sachsen');
INSERT INTO stg_dim_state VALUES (6, 0, 'Hessen');
INSERT INTO stg_dim_state VALUES (4, 0, 'Bremen');
INSERT INTO stg_dim_state VALUES (16, 0, 'Thüringen');
INSERT INTO stg_dim_state VALUES (12, 0, 'Brandenburg');

CREATE TABLE STG_DIM_COUNTY (
	ID smallint,
	STATE_ID smallint,
	NAME varchar(128),
	PRIMARY KEY (ID),
	FOREIGN KEY (STATE_ID) REFERENCES STG_DIM_STATE (ID),
	UNIQUE (NAME, STATE_ID)
);

INSERT INTO stg_dim_county VALUES (15088, 15, 'LK Saalekreis');
INSERT INTO stg_dim_county VALUES (3102, 3, 'SK Salzgitter');
INSERT INTO stg_dim_county VALUES (3405, 3, 'SK Wilhelmshaven');
INSERT INTO stg_dim_county VALUES (16056, 16, 'SK Eisenach');
INSERT INTO stg_dim_county VALUES (9672, 9, 'LK Bad Kissingen');
INSERT INTO stg_dim_county VALUES (5954, 5, 'LK Ennepe-Ruhr-Kreis');
INSERT INTO stg_dim_county VALUES (7338, 7, 'LK Rhein-Pfalz-Kreis');
INSERT INTO stg_dim_county VALUES (9778, 9, 'LK Unterallgäu');
INSERT INTO stg_dim_county VALUES (3251, 3, 'LK Diepholz');
INSERT INTO stg_dim_county VALUES (16071, 16, 'LK Weimarer Land');
INSERT INTO stg_dim_county VALUES (9764, 9, 'SK Memmingen');
INSERT INTO stg_dim_county VALUES (5358, 5, 'LK Düren');
INSERT INTO stg_dim_county VALUES (13004, 13, 'SK Schwerin');
INSERT INTO stg_dim_county VALUES (8216, 8, 'LK Rastatt');
INSERT INTO stg_dim_county VALUES (8222, 8, 'SK Mannheim');
INSERT INTO stg_dim_county VALUES (7320, 7, 'SK Zweibrücken');
INSERT INTO stg_dim_county VALUES (10043, 10, 'LK Neunkirchen');
INSERT INTO stg_dim_county VALUES (9471, 9, 'LK Bamberg');
INSERT INTO stg_dim_county VALUES (5970, 5, 'LK Siegen-Wittgenstein');
INSERT INTO stg_dim_county VALUES (16061, 16, 'LK Eichsfeld');
INSERT INTO stg_dim_county VALUES (12071, 12, 'LK Spree-Neiße');
INSERT INTO stg_dim_county VALUES (1059, 1, 'LK Schleswig-Flensburg');
INSERT INTO stg_dim_county VALUES (9464, 9, 'SK Hof');
INSERT INTO stg_dim_county VALUES (15081, 15, 'LK Altmarkkreis Salzwedel');
INSERT INTO stg_dim_county VALUES (9678, 9, 'LK Schweinfurt');
INSERT INTO stg_dim_county VALUES (3357, 3, 'LK Rotenburg (Wümme)');
INSERT INTO stg_dim_county VALUES (8116, 8, 'LK Esslingen');
INSERT INTO stg_dim_county VALUES (9463, 9, 'SK Coburg');
INSERT INTO stg_dim_county VALUES (5122, 5, 'SK Solingen');
INSERT INTO stg_dim_county VALUES (15083, 15, 'LK Börde');
INSERT INTO stg_dim_county VALUES (5562, 5, 'LK Recklinghausen');
INSERT INTO stg_dim_county VALUES (9161, 9, 'SK Ingolstadt');
INSERT INTO stg_dim_county VALUES (9377, 9, 'LK Tirschenreuth');
INSERT INTO stg_dim_county VALUES (8231, 8, 'SK Pforzheim');
INSERT INTO stg_dim_county VALUES (9375, 9, 'LK Regensburg');
INSERT INTO stg_dim_county VALUES (7317, 7, 'SK Pirmasens');
INSERT INTO stg_dim_county VALUES (14625, 14, 'LK Bautzen');
INSERT INTO stg_dim_county VALUES (7311, 7, 'SK Frankenthal');
INSERT INTO stg_dim_county VALUES (12070, 12, 'LK Prignitz');
INSERT INTO stg_dim_county VALUES (9574, 9, 'LK Nürnberger Land');
INSERT INTO stg_dim_county VALUES (8426, 8, 'LK Biberach');
INSERT INTO stg_dim_county VALUES (3401, 3, 'SK Delmenhorst');
INSERT INTO stg_dim_county VALUES (9661, 9, 'SK Aschaffenburg');
INSERT INTO stg_dim_county VALUES (10041, 10, 'LK Stadtverband Saarbrücken');
INSERT INTO stg_dim_county VALUES (9564, 9, 'SK Nürnberg');
INSERT INTO stg_dim_county VALUES (3358, 3, 'LK Heidekreis');
INSERT INTO stg_dim_county VALUES (6438, 6, 'LK Offenbach');
INSERT INTO stg_dim_county VALUES (12067, 12, 'LK Oder-Spree');
INSERT INTO stg_dim_county VALUES (5117, 5, 'SK Mülheim a.d.Ruhr');
INSERT INTO stg_dim_county VALUES (1053, 1, 'LK Herzogtum Lauenburg');
INSERT INTO stg_dim_county VALUES (6439, 6, 'LK Rheingau-Taunus-Kreis');
INSERT INTO stg_dim_county VALUES (3454, 3, 'LK Emsland');
INSERT INTO stg_dim_county VALUES (6535, 6, 'LK Vogelsbergkreis');
INSERT INTO stg_dim_county VALUES (5378, 5, 'LK Rheinisch-Bergischer Kreis');
INSERT INTO stg_dim_county VALUES (13072, 13, 'LK Rostock');
INSERT INTO stg_dim_county VALUES (16063, 16, 'LK Wartburgkreis');
INSERT INTO stg_dim_county VALUES (7334, 7, 'LK Germersheim');
INSERT INTO stg_dim_county VALUES (6432, 6, 'LK Darmstadt-Dieburg');
INSERT INTO stg_dim_county VALUES (5962, 5, 'LK Märkischer Kreis');
INSERT INTO stg_dim_county VALUES (9475, 9, 'LK Hof');
INSERT INTO stg_dim_county VALUES (3241, 3, 'Region Hannover');
INSERT INTO stg_dim_county VALUES (9671, 9, 'LK Aschaffenburg');
INSERT INTO stg_dim_county VALUES (5913, 5, 'SK Dortmund');
INSERT INTO stg_dim_county VALUES (9679, 9, 'LK Würzburg');
INSERT INTO stg_dim_county VALUES (5116, 5, 'SK Mönchengladbach');
INSERT INTO stg_dim_county VALUES (8215, 8, 'LK Karlsruhe');
INSERT INTO stg_dim_county VALUES (4012, 4, 'SK Bremerhaven');
INSERT INTO stg_dim_county VALUES (12051, 12, 'SK Brandenburg a.d.Havel');
INSERT INTO stg_dim_county VALUES (12065, 12, 'LK Oberhavel');
INSERT INTO stg_dim_county VALUES (5958, 5, 'LK Hochsauerlandkreis');
INSERT INTO stg_dim_county VALUES (9774, 9, 'LK Günzburg');
INSERT INTO stg_dim_county VALUES (3353, 3, 'LK Harburg');
INSERT INTO stg_dim_county VALUES (10045, 10, 'LK Saar-Pfalz-Kreis');
INSERT INTO stg_dim_county VALUES (9376, 9, 'LK Schwandorf');
INSERT INTO stg_dim_county VALUES (7137, 7, 'LK Mayen-Koblenz');
INSERT INTO stg_dim_county VALUES (7332, 7, 'LK Bad Dürkheim');
INSERT INTO stg_dim_county VALUES (15001, 15, 'SK Dessau-Roßlau');
INSERT INTO stg_dim_county VALUES (3460, 3, 'LK Vechta');
INSERT INTO stg_dim_county VALUES (14612, 14, 'SK Dresden');
INSERT INTO stg_dim_county VALUES (5162, 5, 'LK Rhein-Kreis Neuss');
INSERT INTO stg_dim_county VALUES (9677, 9, 'LK Main-Spessart');
INSERT INTO stg_dim_county VALUES (9676, 9, 'LK Miltenberg');
INSERT INTO stg_dim_county VALUES (8336, 8, 'LK Lörrach');
INSERT INTO stg_dim_county VALUES (9277, 9, 'LK Rottal-Inn');
INSERT INTO stg_dim_county VALUES (7135, 7, 'LK Cochem-Zell');
INSERT INTO stg_dim_county VALUES (1003, 1, 'SK Lübeck');
INSERT INTO stg_dim_county VALUES (11007, 11, 'SK Berlin Tempelhof-Schöneberg');
INSERT INTO stg_dim_county VALUES (7336, 7, 'LK Kusel');
INSERT INTO stg_dim_county VALUES (3461, 3, 'LK Wesermarsch');
INSERT INTO stg_dim_county VALUES (13076, 13, 'LK Ludwigslust-Parchim');
INSERT INTO stg_dim_county VALUES (9779, 9, 'LK Donau-Ries');
INSERT INTO stg_dim_county VALUES (9373, 9, 'LK Neumarkt i.d.OPf.');
INSERT INTO stg_dim_county VALUES (7133, 7, 'LK Bad Kreuznach');
INSERT INTO stg_dim_county VALUES (5711, 5, 'SK Bielefeld');
INSERT INTO stg_dim_county VALUES (5916, 5, 'SK Herne');
INSERT INTO stg_dim_county VALUES (7318, 7, 'SK Speyer');
INSERT INTO stg_dim_county VALUES (1055, 1, 'LK Ostholstein');
INSERT INTO stg_dim_county VALUES (5158, 5, 'LK Mettmann');
INSERT INTO stg_dim_county VALUES (9276, 9, 'LK Regen');
INSERT INTO stg_dim_county VALUES (3457, 3, 'LK Leer');
INSERT INTO stg_dim_county VALUES (3356, 3, 'LK Osterholz');
INSERT INTO stg_dim_county VALUES (9477, 9, 'LK Kulmbach');
INSERT INTO stg_dim_county VALUES (9172, 9, 'LK Berchtesgadener Land');
INSERT INTO stg_dim_county VALUES (5914, 5, 'SK Hagen');
INSERT INTO stg_dim_county VALUES (3101, 3, 'SK Braunschweig');
INSERT INTO stg_dim_county VALUES (9674, 9, 'LK Haßberge');
INSERT INTO stg_dim_county VALUES (6412, 6, 'SK Frankfurt am Main');
INSERT INTO stg_dim_county VALUES (8126, 8, 'LK Hohenlohekreis');
INSERT INTO stg_dim_county VALUES (12064, 12, 'LK Märkisch-Oderland');
INSERT INTO stg_dim_county VALUES (8235, 8, 'LK Calw');
INSERT INTO stg_dim_county VALUES (9187, 9, 'LK Rosenheim');
INSERT INTO stg_dim_county VALUES (9662, 9, 'SK Schweinfurt');
INSERT INTO stg_dim_county VALUES (3255, 3, 'LK Holzminden');
INSERT INTO stg_dim_county VALUES (3355, 3, 'LK Lüneburg');
INSERT INTO stg_dim_county VALUES (3402, 3, 'SK Emden');
INSERT INTO stg_dim_county VALUES (8317, 8, 'LK Ortenaukreis');
INSERT INTO stg_dim_county VALUES (15085, 15, 'LK Harz');
INSERT INTO stg_dim_county VALUES (9263, 9, 'SK Straubing');
INSERT INTO stg_dim_county VALUES (8211, 8, 'SK Baden-Baden');
INSERT INTO stg_dim_county VALUES (9562, 9, 'SK Erlangen');
INSERT INTO stg_dim_county VALUES (5762, 5, 'LK Höxter');
INSERT INTO stg_dim_county VALUES (9361, 9, 'SK Amberg');
INSERT INTO stg_dim_county VALUES (9181, 9, 'LK Landsberg a.Lech');
INSERT INTO stg_dim_county VALUES (8417, 8, 'LK Zollernalbkreis');
INSERT INTO stg_dim_county VALUES (8416, 8, 'LK Tübingen');
INSERT INTO stg_dim_county VALUES (6534, 6, 'LK Marburg-Biedenkopf');
INSERT INTO stg_dim_county VALUES (7143, 7, 'LK Westerwaldkreis');
INSERT INTO stg_dim_county VALUES (3252, 3, 'LK Hameln-Pyrmont');
INSERT INTO stg_dim_county VALUES (8435, 8, 'LK Bodenseekreis');
INSERT INTO stg_dim_county VALUES (9362, 9, 'SK Regensburg');
INSERT INTO stg_dim_county VALUES (7140, 7, 'LK Rhein-Hunsrück-Kreis');
INSERT INTO stg_dim_county VALUES (14523, 14, 'LK Vogtlandkreis');
INSERT INTO stg_dim_county VALUES (3351, 3, 'LK Celle');
INSERT INTO stg_dim_county VALUES (6435, 6, 'LK Main-Kinzig-Kreis');
INSERT INTO stg_dim_county VALUES (16076, 16, 'LK Greiz');
INSERT INTO stg_dim_county VALUES (16069, 16, 'LK Hildburghausen');
INSERT INTO stg_dim_county VALUES (1062, 1, 'LK Stormarn');
INSERT INTO stg_dim_county VALUES (15084, 15, 'LK Burgenlandkreis');
INSERT INTO stg_dim_county VALUES (15003, 15, 'SK Magdeburg');
INSERT INTO stg_dim_county VALUES (9180, 9, 'LK Garmisch-Partenkirchen');
INSERT INTO stg_dim_county VALUES (8335, 8, 'LK Konstanz');
INSERT INTO stg_dim_county VALUES (5754, 5, 'LK Gütersloh');
INSERT INTO stg_dim_county VALUES (9190, 9, 'LK Weilheim-Schongau');
INSERT INTO stg_dim_county VALUES (1002, 1, 'SK Kiel');
INSERT INTO stg_dim_county VALUES (9177, 9, 'LK Erding');
INSERT INTO stg_dim_county VALUES (7340, 7, 'LK Südwestpfalz');
INSERT INTO stg_dim_county VALUES (5774, 5, 'LK Paderborn');
INSERT INTO stg_dim_county VALUES (12053, 12, 'SK Frankfurt (Oder)');
INSERT INTO stg_dim_county VALUES (9478, 9, 'LK Lichtenfels');
INSERT INTO stg_dim_county VALUES (1054, 1, 'LK Nordfriesland');
INSERT INTO stg_dim_county VALUES (1058, 1, 'LK Rendsburg-Eckernförde');
INSERT INTO stg_dim_county VALUES (16054, 16, 'SK Suhl');
INSERT INTO stg_dim_county VALUES (5370, 5, 'LK Heinsberg');
INSERT INTO stg_dim_county VALUES (9761, 9, 'SK Augsburg');
INSERT INTO stg_dim_county VALUES (2000, 2, 'SK Hamburg');
INSERT INTO stg_dim_county VALUES (13003, 13, 'SK Rostock');
INSERT INTO stg_dim_county VALUES (11008, 11, 'SK Berlin Neukölln');
INSERT INTO stg_dim_county VALUES (7333, 7, 'LK Donnersbergkreis');
INSERT INTO stg_dim_county VALUES (5170, 5, 'LK Wesel');
INSERT INTO stg_dim_county VALUES (9173, 9, 'LK Bad Tölz-Wolfratshausen');
INSERT INTO stg_dim_county VALUES (5974, 5, 'LK Soest');
INSERT INTO stg_dim_county VALUES (6431, 6, 'LK Bergstraße');
INSERT INTO stg_dim_county VALUES (11002, 11, 'SK Berlin Friedrichshain-Kreuzberg');
INSERT INTO stg_dim_county VALUES (7313, 7, 'SK Landau i.d.Pfalz');
INSERT INTO stg_dim_county VALUES (5570, 5, 'LK Warendorf');
INSERT INTO stg_dim_county VALUES (14628, 14, 'LK Sächsische Schweiz-Osterzgebirge');
INSERT INTO stg_dim_county VALUES (8237, 8, 'LK Freudenstadt');
INSERT INTO stg_dim_county VALUES (9776, 9, 'LK Lindau');
INSERT INTO stg_dim_county VALUES (3354, 3, 'LK Lüchow-Dannenberg');
INSERT INTO stg_dim_county VALUES (8115, 8, 'LK Böblingen');
INSERT INTO stg_dim_county VALUES (5554, 5, 'LK Borken');
INSERT INTO stg_dim_county VALUES (8226, 8, 'LK Rhein-Neckar-Kreis');
INSERT INTO stg_dim_county VALUES (16075, 16, 'LK Saale-Orla-Kreis');
INSERT INTO stg_dim_county VALUES (6531, 6, 'LK Gießen');
INSERT INTO stg_dim_county VALUES (5978, 5, 'LK Unna');
INSERT INTO stg_dim_county VALUES (9573, 9, 'LK Fürth');
INSERT INTO stg_dim_county VALUES (11005, 11, 'SK Berlin Spandau');
INSERT INTO stg_dim_county VALUES (6636, 6, 'LK Werra-Meißner-Kreis');
INSERT INTO stg_dim_county VALUES (15091, 15, 'LK Wittenberg');
INSERT INTO stg_dim_county VALUES (11001, 11, 'SK Berlin Mitte');
INSERT INTO stg_dim_county VALUES (3451, 3, 'LK Ammerland');
INSERT INTO stg_dim_county VALUES (9571, 9, 'LK Ansbach');
INSERT INTO stg_dim_county VALUES (9275, 9, 'LK Passau');
INSERT INTO stg_dim_county VALUES (16066, 16, 'LK Schmalkalden-Meiningen');
INSERT INTO stg_dim_county VALUES (16070, 16, 'LK Ilm-Kreis');
INSERT INTO stg_dim_county VALUES (8127, 8, 'LK Schwäbisch Hall');
INSERT INTO stg_dim_county VALUES (10044, 10, 'LK Saarlouis');
INSERT INTO stg_dim_county VALUES (6440, 6, 'LK Wetteraukreis');
INSERT INTO stg_dim_county VALUES (5314, 5, 'SK Bonn');
INSERT INTO stg_dim_county VALUES (3456, 3, 'LK Grafschaft Bentheim');
INSERT INTO stg_dim_county VALUES (9371, 9, 'LK Amberg-Sulzbach');
INSERT INTO stg_dim_county VALUES (16062, 16, 'LK Nordhausen');
INSERT INTO stg_dim_county VALUES (12063, 12, 'LK Havelland');
INSERT INTO stg_dim_county VALUES (12066, 12, 'LK Oberspreewald-Lausitz');
INSERT INTO stg_dim_county VALUES (8415, 8, 'LK Reutlingen');
INSERT INTO stg_dim_county VALUES (3404, 3, 'SK Osnabrück');
INSERT INTO stg_dim_county VALUES (11011, 11, 'SK Berlin Lichtenberg');
INSERT INTO stg_dim_county VALUES (6632, 6, 'LK Hersfeld-Rotenburg');
INSERT INTO stg_dim_county VALUES (6434, 6, 'LK Hochtaunuskreis');
INSERT INTO stg_dim_county VALUES (3151, 3, 'LK Gifhorn');
INSERT INTO stg_dim_county VALUES (7312, 7, 'SK Kaiserslautern');
INSERT INTO stg_dim_county VALUES (9163, 9, 'SK Rosenheim');
INSERT INTO stg_dim_county VALUES (7141, 7, 'LK Rhein-Lahn-Kreis');
INSERT INTO stg_dim_county VALUES (8337, 8, 'LK Waldshut');
INSERT INTO stg_dim_county VALUES (8225, 8, 'LK Neckar-Odenwald-Kreis');
INSERT INTO stg_dim_county VALUES (9675, 9, 'LK Kitzingen');
INSERT INTO stg_dim_county VALUES (3458, 3, 'LK Oldenburg');
INSERT INTO stg_dim_county VALUES (15002, 15, 'SK Halle');
INSERT INTO stg_dim_county VALUES (6633, 6, 'LK Kassel');
INSERT INTO stg_dim_county VALUES (9563, 9, 'SK Fürth');
INSERT INTO stg_dim_county VALUES (9178, 9, 'LK Freising');
INSERT INTO stg_dim_county VALUES (3256, 3, 'LK Nienburg (Weser)');
INSERT INTO stg_dim_county VALUES (5366, 5, 'LK Euskirchen');
INSERT INTO stg_dim_county VALUES (9775, 9, 'LK Neu-Ulm');
INSERT INTO stg_dim_county VALUES (16072, 16, 'LK Sonneberg');
INSERT INTO stg_dim_county VALUES (3453, 3, 'LK Cloppenburg');
INSERT INTO stg_dim_county VALUES (7134, 7, 'LK Birkenfeld');
INSERT INTO stg_dim_county VALUES (9186, 9, 'LK Pfaffenhofen a.d.Ilm');
INSERT INTO stg_dim_county VALUES (16053, 16, 'SK Jena');
INSERT INTO stg_dim_county VALUES (9663, 9, 'SK Würzburg');
INSERT INTO stg_dim_county VALUES (5114, 5, 'SK Krefeld');
INSERT INTO stg_dim_county VALUES (3459, 3, 'LK Osnabrück');
INSERT INTO stg_dim_county VALUES (6433, 6, 'LK Groß-Gerau');
INSERT INTO stg_dim_county VALUES (7232, 7, 'LK Bitburg-Prüm');
INSERT INTO stg_dim_county VALUES (7233, 7, 'LK Vulkaneifel');
INSERT INTO stg_dim_county VALUES (9183, 9, 'LK Mühldorf a.Inn');
INSERT INTO stg_dim_county VALUES (9261, 9, 'SK Landshut');
INSERT INTO stg_dim_county VALUES (9176, 9, 'LK Eichstätt');
INSERT INTO stg_dim_county VALUES (6532, 6, 'LK Lahn-Dill-Kreis');
INSERT INTO stg_dim_county VALUES (8121, 8, 'SK Heilbronn');
INSERT INTO stg_dim_county VALUES (1004, 1, 'SK Neumünster');
INSERT INTO stg_dim_county VALUES (3462, 3, 'LK Wittmund');
INSERT INTO stg_dim_county VALUES (8421, 8, 'SK Ulm');
INSERT INTO stg_dim_county VALUES (7319, 7, 'SK Worms');
INSERT INTO stg_dim_county VALUES (16077, 16, 'LK Altenburger Land');
INSERT INTO stg_dim_county VALUES (3254, 3, 'LK Hildesheim');
INSERT INTO stg_dim_county VALUES (8327, 8, 'LK Tuttlingen');
INSERT INTO stg_dim_county VALUES (12052, 12, 'SK Cottbus');
INSERT INTO stg_dim_county VALUES (11010, 11, 'SK Berlin Marzahn-Hellersdorf');
INSERT INTO stg_dim_county VALUES (5513, 5, 'SK Gelsenkirchen');
INSERT INTO stg_dim_county VALUES (5766, 5, 'LK Lippe');
INSERT INTO stg_dim_county VALUES (3403, 3, 'SK Oldenburg');
INSERT INTO stg_dim_county VALUES (7235, 7, 'LK Trier-Saarburg');
INSERT INTO stg_dim_county VALUES (8212, 8, 'SK Karlsruhe');
INSERT INTO stg_dim_county VALUES (3157, 3, 'LK Peine');
INSERT INTO stg_dim_county VALUES (14730, 14, 'LK Nordsachsen');
INSERT INTO stg_dim_county VALUES (1057, 1, 'LK Plön');
INSERT INTO stg_dim_county VALUES (8136, 8, 'LK Ostalbkreis');
INSERT INTO stg_dim_county VALUES (8315, 8, 'LK Breisgau-Hochschwarzwald');
INSERT INTO stg_dim_county VALUES (8128, 8, 'LK Main-Tauber-Kreis');
INSERT INTO stg_dim_county VALUES (7314, 7, 'SK Ludwigshafen');
INSERT INTO stg_dim_county VALUES (9179, 9, 'LK Fürstenfeldbruck');
INSERT INTO stg_dim_county VALUES (5166, 5, 'LK Viersen');
INSERT INTO stg_dim_county VALUES (15082, 15, 'LK Anhalt-Bitterfeld');
INSERT INTO stg_dim_county VALUES (7138, 7, 'LK Neuwied');
INSERT INTO stg_dim_county VALUES (6631, 6, 'LK Fulda');
INSERT INTO stg_dim_county VALUES (13073, 13, 'LK Vorpommern-Rügen');
INSERT INTO stg_dim_county VALUES (9363, 9, 'SK Weiden i.d.OPf.');
INSERT INTO stg_dim_county VALUES (3359, 3, 'LK Stade');
INSERT INTO stg_dim_county VALUES (5512, 5, 'SK Bottrop');
INSERT INTO stg_dim_county VALUES (9762, 9, 'SK Kaufbeuren');
INSERT INTO stg_dim_county VALUES (5119, 5, 'SK Oberhausen');
INSERT INTO stg_dim_county VALUES (7337, 7, 'LK Südliche Weinstraße');
INSERT INTO stg_dim_county VALUES (3352, 3, 'LK Cuxhaven');
INSERT INTO stg_dim_county VALUES (14729, 14, 'LK Leipzig');
INSERT INTO stg_dim_county VALUES (9476, 9, 'LK Kronach');
INSERT INTO stg_dim_county VALUES (9771, 9, 'LK Aichach-Friedberg');
INSERT INTO stg_dim_county VALUES (3257, 3, 'LK Schaumburg');
INSERT INTO stg_dim_county VALUES (14627, 14, 'LK Meißen');
INSERT INTO stg_dim_county VALUES (14524, 14, 'LK Zwickau');
INSERT INTO stg_dim_county VALUES (7316, 7, 'SK Neustadt a.d.Weinstraße');
INSERT INTO stg_dim_county VALUES (5915, 5, 'SK Hamm');
INSERT INTO stg_dim_county VALUES (13074, 13, 'LK Nordwestmecklenburg');
INSERT INTO stg_dim_county VALUES (5154, 5, 'LK Kleve');
INSERT INTO stg_dim_county VALUES (9184, 9, 'LK München');
INSERT INTO stg_dim_county VALUES (9572, 9, 'LK Erlangen-Höchstadt');
INSERT INTO stg_dim_county VALUES (8425, 8, 'LK Alb-Donau-Kreis');
INSERT INTO stg_dim_county VALUES (16068, 16, 'LK Sömmerda');
INSERT INTO stg_dim_county VALUES (15086, 15, 'LK Jerichower Land');
INSERT INTO stg_dim_county VALUES (3154, 3, 'LK Helmstedt');
INSERT INTO stg_dim_county VALUES (9182, 9, 'LK Miesbach');
INSERT INTO stg_dim_county VALUES (15089, 15, 'LK Salzlandkreis');
INSERT INTO stg_dim_county VALUES (5770, 5, 'LK Minden-Lübbecke');
INSERT INTO stg_dim_county VALUES (14521, 14, 'LK Erzgebirgskreis');
INSERT INTO stg_dim_county VALUES (5334, 5, 'StadtRegion Aachen');
INSERT INTO stg_dim_county VALUES (9274, 9, 'LK Landshut');
INSERT INTO stg_dim_county VALUES (3153, 3, 'LK Goslar');
INSERT INTO stg_dim_county VALUES (5374, 5, 'LK Oberbergischer Kreis');
INSERT INTO stg_dim_county VALUES (16074, 16, 'LK Saale-Holzland-Kreis');
INSERT INTO stg_dim_county VALUES (3103, 3, 'SK Wolfsburg');
INSERT INTO stg_dim_county VALUES (8125, 8, 'LK Heilbronn');
INSERT INTO stg_dim_county VALUES (7231, 7, 'LK Bernkastel-Wittlich');
INSERT INTO stg_dim_county VALUES (10042, 10, 'LK Merzig-Wadern');
INSERT INTO stg_dim_county VALUES (6411, 6, 'SK Darmstadt');
INSERT INTO stg_dim_county VALUES (3159, 3, 'LK Göttingen');
INSERT INTO stg_dim_county VALUES (8436, 8, 'LK Ravensburg');
INSERT INTO stg_dim_county VALUES (9773, 9, 'LK Dillingen a.d.Donau');
INSERT INTO stg_dim_county VALUES (8135, 8, 'LK Heidenheim');
INSERT INTO stg_dim_county VALUES (10046, 10, 'LK Sankt Wendel');
INSERT INTO stg_dim_county VALUES (3360, 3, 'LK Uelzen');
INSERT INTO stg_dim_county VALUES (11006, 11, 'SK Berlin Steglitz-Zehlendorf');
INSERT INTO stg_dim_county VALUES (4011, 4, 'SK Bremen');
INSERT INTO stg_dim_county VALUES (7211, 7, 'SK Trier');
INSERT INTO stg_dim_county VALUES (6437, 6, 'LK Odenwaldkreis');
INSERT INTO stg_dim_county VALUES (6611, 6, 'SK Kassel');
INSERT INTO stg_dim_county VALUES (12054, 12, 'SK Potsdam');
INSERT INTO stg_dim_county VALUES (9474, 9, 'LK Forchheim');
INSERT INTO stg_dim_county VALUES (14522, 14, 'LK Mittelsachsen');
INSERT INTO stg_dim_county VALUES (16067, 16, 'LK Gotha');
INSERT INTO stg_dim_county VALUES (14713, 14, 'SK Leipzig');
INSERT INTO stg_dim_county VALUES (5315, 5, 'SK Köln');
INSERT INTO stg_dim_county VALUES (5112, 5, 'SK Duisburg');
INSERT INTO stg_dim_county VALUES (12069, 12, 'LK Potsdam-Mittelmark');
INSERT INTO stg_dim_county VALUES (12062, 12, 'LK Elbe-Elster');
INSERT INTO stg_dim_county VALUES (1060, 1, 'LK Segeberg');
INSERT INTO stg_dim_county VALUES (7331, 7, 'LK Alzey-Worms');
INSERT INTO stg_dim_county VALUES (9673, 9, 'LK Rhön-Grabfeld');
INSERT INTO stg_dim_county VALUES (5382, 5, 'LK Rhein-Sieg-Kreis');
INSERT INTO stg_dim_county VALUES (16051, 16, 'SK Erfurt');
INSERT INTO stg_dim_county VALUES (11009, 11, 'SK Berlin Treptow-Köpenick');
INSERT INTO stg_dim_county VALUES (9780, 9, 'LK Oberallgäu');
INSERT INTO stg_dim_county VALUES (8221, 8, 'SK Heidelberg');
INSERT INTO stg_dim_county VALUES (8316, 8, 'LK Emmendingen');
INSERT INTO stg_dim_county VALUES (5113, 5, 'SK Essen');
INSERT INTO stg_dim_county VALUES (6413, 6, 'SK Offenbach');
INSERT INTO stg_dim_county VALUES (3155, 3, 'LK Northeim');
INSERT INTO stg_dim_county VALUES (3452, 3, 'LK Aurich');
INSERT INTO stg_dim_county VALUES (8118, 8, 'LK Ludwigsburg');
INSERT INTO stg_dim_county VALUES (6533, 6, 'LK Limburg-Weilburg');
INSERT INTO stg_dim_county VALUES (13071, 13, 'LK Mecklenburgische Seenplatte');
INSERT INTO stg_dim_county VALUES (9575, 9, 'LK Neustadt a.d.Aisch-Bad Windsheim');
INSERT INTO stg_dim_county VALUES (16065, 16, 'LK Kyffhäuserkreis');
INSERT INTO stg_dim_county VALUES (9271, 9, 'LK Deggendorf');
INSERT INTO stg_dim_county VALUES (3158, 3, 'LK Wolfenbüttel');
INSERT INTO stg_dim_county VALUES (12073, 12, 'LK Uckermark');
INSERT INTO stg_dim_county VALUES (9188, 9, 'LK Starnberg');
INSERT INTO stg_dim_county VALUES (7339, 7, 'LK Mainz-Bingen');
INSERT INTO stg_dim_county VALUES (9175, 9, 'LK Ebersberg');
INSERT INTO stg_dim_county VALUES (8311, 8, 'SK Freiburg i.Breisgau');
INSERT INTO stg_dim_county VALUES (9279, 9, 'LK Dingolfing-Landau');
INSERT INTO stg_dim_county VALUES (5911, 5, 'SK Bochum');
INSERT INTO stg_dim_county VALUES (12061, 12, 'LK Dahme-Spreewald');
INSERT INTO stg_dim_county VALUES (12060, 12, 'LK Barnim');
INSERT INTO stg_dim_county VALUES (8326, 8, 'LK Schwarzwald-Baar-Kreis');
INSERT INTO stg_dim_county VALUES (1056, 1, 'LK Pinneberg');
INSERT INTO stg_dim_county VALUES (9777, 9, 'LK Ostallgäu');
INSERT INTO stg_dim_county VALUES (5758, 5, 'LK Herford');
INSERT INTO stg_dim_county VALUES (6635, 6, 'LK Waldeck-Frankenberg');
INSERT INTO stg_dim_county VALUES (5124, 5, 'SK Wuppertal');
INSERT INTO stg_dim_county VALUES (7315, 7, 'SK Mainz');
INSERT INTO stg_dim_county VALUES (14626, 14, 'LK Görlitz');
INSERT INTO stg_dim_county VALUES (5558, 5, 'LK Coesfeld');
INSERT INTO stg_dim_county VALUES (13075, 13, 'LK Vorpommern-Greifswald');
INSERT INTO stg_dim_county VALUES (16073, 16, 'LK Saalfeld-Rudolstadt');
INSERT INTO stg_dim_county VALUES (8325, 8, 'LK Rottweil');
INSERT INTO stg_dim_county VALUES (1001, 1, 'SK Flensburg');
INSERT INTO stg_dim_county VALUES (9372, 9, 'LK Cham');
INSERT INTO stg_dim_county VALUES (8111, 8, 'SK Stuttgart');
INSERT INTO stg_dim_county VALUES (11003, 11, 'SK Berlin Pankow');
INSERT INTO stg_dim_county VALUES (9189, 9, 'LK Traunstein');
INSERT INTO stg_dim_county VALUES (9262, 9, 'SK Passau');
INSERT INTO stg_dim_county VALUES (9185, 9, 'LK Neuburg-Schrobenhausen');
INSERT INTO stg_dim_county VALUES (3455, 3, 'LK Friesland');
INSERT INTO stg_dim_county VALUES (8437, 8, 'LK Sigmaringen');
INSERT INTO stg_dim_county VALUES (1061, 1, 'LK Steinburg');
INSERT INTO stg_dim_county VALUES (16064, 16, 'LK Unstrut-Hainich-Kreis');
INSERT INTO stg_dim_county VALUES (5515, 5, 'SK Münster');
INSERT INTO stg_dim_county VALUES (7335, 7, 'LK Kaiserslautern');
INSERT INTO stg_dim_county VALUES (16052, 16, 'SK Gera');
INSERT INTO stg_dim_county VALUES (11004, 11, 'SK Berlin Charlottenburg-Wilmersdorf');
INSERT INTO stg_dim_county VALUES (9472, 9, 'LK Bayreuth');
INSERT INTO stg_dim_county VALUES (5316, 5, 'SK Leverkusen');
INSERT INTO stg_dim_county VALUES (3361, 3, 'LK Verden');
INSERT INTO stg_dim_county VALUES (14511, 14, 'SK Chemnitz');
INSERT INTO stg_dim_county VALUES (5120, 5, 'SK Remscheid');
INSERT INTO stg_dim_county VALUES (9162, 9, 'SK München');
INSERT INTO stg_dim_county VALUES (5966, 5, 'LK Olpe');
INSERT INTO stg_dim_county VALUES (5111, 5, 'SK Düsseldorf');
INSERT INTO stg_dim_county VALUES (11012, 11, 'SK Berlin Reinickendorf');
INSERT INTO stg_dim_county VALUES (9772, 9, 'LK Augsburg');
INSERT INTO stg_dim_county VALUES (8236, 8, 'LK Enzkreis');
INSERT INTO stg_dim_county VALUES (9763, 9, 'SK Kempten');
INSERT INTO stg_dim_county VALUES (9473, 9, 'LK Coburg');
INSERT INTO stg_dim_county VALUES (15087, 15, 'LK Mansfeld-Südharz');
INSERT INTO stg_dim_county VALUES (1051, 1, 'LK Dithmarschen');
INSERT INTO stg_dim_county VALUES (9278, 9, 'LK Straubing-Bogen');
INSERT INTO stg_dim_county VALUES (9576, 9, 'LK Roth');
INSERT INTO stg_dim_county VALUES (9577, 9, 'LK Weißenburg-Gunzenhausen');
INSERT INTO stg_dim_county VALUES (12072, 12, 'LK Teltow-Fläming');
INSERT INTO stg_dim_county VALUES (9461, 9, 'SK Bamberg');
INSERT INTO stg_dim_county VALUES (9273, 9, 'LK Kelheim');
INSERT INTO stg_dim_county VALUES (5566, 5, 'LK Steinfurt');
INSERT INTO stg_dim_county VALUES (5362, 5, 'LK Rhein-Erft-Kreis');
INSERT INTO stg_dim_county VALUES (9561, 9, 'SK Ansbach');
INSERT INTO stg_dim_county VALUES (16055, 16, 'SK Weimar');
INSERT INTO stg_dim_county VALUES (7132, 7, 'LK Altenkirchen');
INSERT INTO stg_dim_county VALUES (6436, 6, 'LK Main-Taunus-Kreis');
INSERT INTO stg_dim_county VALUES (6414, 6, 'SK Wiesbaden');
INSERT INTO stg_dim_county VALUES (9479, 9, 'LK Wunsiedel i.Fichtelgebirge');
INSERT INTO stg_dim_county VALUES (9171, 9, 'LK Altötting');
INSERT INTO stg_dim_county VALUES (8119, 8, 'LK Rems-Murr-Kreis');
INSERT INTO stg_dim_county VALUES (7111, 7, 'SK Koblenz');
INSERT INTO stg_dim_county VALUES (8117, 8, 'LK Göppingen');
INSERT INTO stg_dim_county VALUES (9174, 9, 'LK Dachau');
INSERT INTO stg_dim_county VALUES (9272, 9, 'LK Freyung-Grafenau');
INSERT INTO stg_dim_county VALUES (15090, 15, 'LK Stendal');
INSERT INTO stg_dim_county VALUES (7131, 7, 'LK Ahrweiler');
INSERT INTO stg_dim_county VALUES (9374, 9, 'LK Neustadt a.d.Waldnaab');
INSERT INTO stg_dim_county VALUES (9565, 9, 'SK Schwabach');
INSERT INTO stg_dim_county VALUES (6634, 6, 'LK Schwalm-Eder-Kreis');
INSERT INTO stg_dim_county VALUES (9462, 9, 'SK Bayreuth');
INSERT INTO stg_dim_county VALUES (12068, 12, 'LK Ostprignitz-Ruppin');

CREATE TABLE STG_DIM_AGEGROUP (
	ID smallint,
	DESCR varchar(24),
	MIN_AGE smallint,
	MAX_AGE smallint,
	PRIMARY KEY (ID),
	UNIQUE (DESCR)
);

insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (0, 'unbekannt', NULL, NULL);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (1, 'Nicht übermittelt', NULL, NULL);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (2, 'A00-A04', 0, 4);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (3, 'A05-A14', 5, 14);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (4, 'A15-A34', 15, 34);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (5, 'A35-A59', 35, 59);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (6, 'A60-A79', 60, 79);
insert into stg_dim_agegroup (ID, DESCR, MIN_AGE, MAX_AGE) values (7, 'A80+', 80, NULL);

CREATE TABLE STG_DIM_GENDER (
	ID smallint,
	NAME varchar(9),
	PRIMARY KEY (ID),
	UNIQUE (NAME)
);
insert into stg_dim_gender (id, name) values (0, 'unbekannt');
insert into stg_dim_gender (id, name) values (1, 'W');
insert into stg_dim_gender (id, name) values (2, 'M');

CREATE TABLE STG_DIM_NEWCASE (
	ID smallint,
	DESCR varchar(128),
	PRIMARY KEY (ID),
	UNIQUE(DESCR)
);
INSERT INTO STG_DIM_NEWCASE VALUES (-1, 'Fall ist nur in der Publikation des Vortags enthalten');
INSERT INTO STG_DIM_NEWCASE VALUES (0, 'Fall ist in der Publikation für den aktuellen Tag und in der für den Vortag enthalten');
INSERT INTO STG_DIM_NEWCASE VALUES (1, 'Fall ist nur in der aktuellen Publikation enthalten');

CREATE TABLE STG_DIM_NEWDEATH (
	ID smallint,
	DESCR varchar(128),
	PRIMARY KEY (ID),
	UNIQUE(DESCR)
);
INSERT INTO STG_DIM_NEWDEATH VALUES (0, 'Fall ist in der Publikation für den aktuellen Tag und in der für den Vortag jeweils ein Todesfall');
INSERT INTO STG_DIM_NEWDEATH VALUES (1, 'Fall ist in der aktuellen Publikation ein Todesfall, nicht jedoch in der Publikation des Vortages');
INSERT INTO STG_DIM_NEWDEATH VALUES (-1, 'Fall ist in der aktuellen Publikation kein Todesfall, jedoch war er in der Publikation des Vortags ein Todesfall');
INSERT INTO STG_DIM_NEWDEATH VALUES (-9, 'Fall ist weder in der aktuellen Publikation noch in der des Vortages ein Todesfall');

CREATE TABLE STG_DIM_NEWRECOV (
	ID smallint,
	DESCR varchar(128),
	PRIMARY KEY (ID),
	UNIQUE(DESCR)
);
INSERT INTO STG_DIM_NEWRECOV VALUES (0, 'Fall ist in der Publikation für den aktuellen Tag und in der für den Vortag jeweils Genesen');
INSERT INTO STG_DIM_NEWRECOV VALUES (1, 'Fall ist in der aktuellen Publikation Genesen, nicht jedoch in der Publikation des Vortages');
INSERT INTO STG_DIM_NEWRECOV VALUES (-1, 'Fall ist in der aktuellen Publikation nicht Genesen, jedoch war er in der Publikation des Vortags Genesen');
INSERT INTO STG_DIM_NEWRECOV VALUES (-9, 'Fall ist weder in der aktuellen Publikation noch in der des Vortages Genesen');

CREATE TABLE STG_RKI (
	INSERT_DATE TIMESTAMP NOT NULL,
	STATE_ID SMALLINT NOT NULL,
	COUNTY_ID SMALLINT NOT NULL,
	AGE_GROUP_ID SMALLINT NOT NULL,
	GENDER_ID SMALLINT NOT NULL,
	REPORT_DATE DATE NOT NULL,
	ONSET_DATE DATE NULL,
	NEWCASE_ID SMALLINT NOT NULL,
	NEWDEATH_ID SMALLINT NOT NULL,
	NEWRECOV_ID SMALLINT NOT NULL,
	CASE_COUNT INTEGER NOT NULL,
	DEATH_COUNT INTEGER NOT NULL,
	RECOV_COUNT SMALLINT NOT NULL,
	PRIMARY KEY (STATE_ID, COUNTY_ID, AGE_GROUP_ID, GENDER_ID, REPORT_DATE, ONSET_DATE, NEWCASE_ID, NEWDEATH_ID, NEWRECOV_ID),
	FOREIGN KEY (STATE_ID) REFERENCES STG_DIM_STATE (ID),
	FOREIGN KEY (COUNTY_ID) REFERENCES STG_DIM_COUNTY (ID),
	FOREIGN KEY (AGE_GROUP_ID) REFERENCES STG_DIM_AGEGROUP (ID),
	FOREIGN KEY (GENDER_ID) REFERENCES STG_DIM_GENDER (ID),
	FOREIGN KEY (NEWCASE_ID) REFERENCES STG_DIM_NEWCASE (ID),
	FOREIGN KEY (NEWDEATH_ID) REFERENCES STG_DIM_NEWDEATH (ID),
	FOREIGN KEY (NEWRECOV_ID) REFERENCES STG_DIM_NEWRECOV (ID)
);

CREATE TABLE SORTED_OUT_RKI (
	INSERT_DATE TIMESTAMP,
	IDBUNDESLAND INTEGER,
	BUNDESLAND VARCHAR(32),
	LANDKREIS VARCHAR(128),
	ALTERSGRUPPE VARCHAR(32),
	GESCHLECHT VARCHAR(16),
	ANZAHLFALL INTEGER,
	ANZAHLTODESFALL INTEGER,
	OBJECTID INTEGER,
	MELDEDATUM VARCHAR(32),
	IDLANDKREIS INTEGER,
	DATENSTAND VARCHAR(32),
	NEUERFALL INTEGER,
	NEUERTODESFALL INTEGER,
	REFDATUM VARCHAR(32),
	NEUGENESEN INTEGER,
	ANZAHLGENESEN INTEGER,
	ISTERKRANKUNGSBEGINN INTEGER,
	ALTERSGRUPPE2 VARCHAR(32),
	SO_REASON VARCHAR(64)
);