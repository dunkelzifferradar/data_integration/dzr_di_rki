CREATE TABLE SOR_DIM_COUNTY_TYPES (
	ID smallint,
	NAME varchar(16),
	PRIMARY KEY (ID)
);
INSERT INTO SOR_DIM_COUNTY_TYPES VALUES (0, 'Kreisfreie Stadt');
INSERT INTO SOR_DIM_COUNTY_TYPES VALUES (1, 'Kreis');
INSERT INTO SOR_DIM_COUNTY_TYPES VALUES (2, 'Landkreis');
INSERT INTO SOR_DIM_COUNTY_TYPES VALUES (3, 'Stadtkreis');
INSERT INTO SOR_DIM_COUNTY_TYPES VALUES (4, 'Regionalverband');

CREATE TABLE SOR_DIM_STATE (
	ID smallint,
	COUNTRY_ID smallint,
	NAME varchar(24),
	ABBREV char(2),
	AREA integer,
	POPULATION integer,
	POPULATION_DENSITY integer,
	FOREIGN_PCTG float,
	PRIMARY KEY (ID),
	FOREIGN KEY (COUNTRY_ID) REFERENCES SOR_DIM_COUNTRY(ID)
);
INSERT INTO sor_dim_state VALUES (8, 0,  'Baden-Württemberg', 'BW', 35751, 11069533, 304, 14.2);
INSERT INTO sor_dim_state VALUES (9, 0,  'Bayern', 'BY', 7055, 13076721, 182, 12.3);
INSERT INTO sor_dim_state VALUES (11, 0,  'Berlin', 'BE', 892, 3644826, 3948, 16.3);
INSERT INTO sor_dim_state VALUES (12, 0,  'Brandenburg', 'BB', 29654, 2511917, 84, 3.6);
INSERT INTO sor_dim_state VALUES (4, 0,  'Bremen', 'HB', 420, 682986, 1599, 15.9);
INSERT INTO sor_dim_state VALUES (2, 0,  'Hamburg', 'HH', 755, 1841179, 2366, 15.4);
INSERT INTO sor_dim_state VALUES (6, 0,  'Hessen', 'HE', 21115, 6265809, 293, 15.1);
INSERT INTO sor_dim_state VALUES (13, 0,  'Mecklenburg-Vorpommern', 'MV', 23212, 1609675, 69, 4);
INSERT INTO sor_dim_state VALUES (3, 0,  'Niedersachsen', 'NI', 47593, 7982448, 167, 8.4);
INSERT INTO sor_dim_state VALUES (5, 0,  'Nordrhein-Westfalen', 'NW', 34113, 17932651, 524, 12.7);
INSERT INTO sor_dim_state VALUES (7, 0,  'Rheinland-Pfalz', 'RP', 19854, 4084844, 204, 9.7);
INSERT INTO sor_dim_state VALUES (10, 0,  'Saarland', 'SL', 2567, 990509, 388, 10.5);
INSERT INTO sor_dim_state VALUES (14, 0,  'Sachsen', 'SN', 18449, 4077937, 221, 4);
INSERT INTO sor_dim_state VALUES (15, 0,  'Sachsen-Anhalt', 'ST', 20452, 2208321, 110, 3.7);
INSERT INTO sor_dim_state VALUES (1, 0,  'Schleswig-Holstein', 'SH', 15802, 2896712, 181, 6.9);
INSERT INTO sor_dim_state VALUES (16, 0,  'Thüringen', 'TH', 16202, 2143145, 134, 3.5);

CREATE TABLE SOR_DIM_COUNTY (
	ID integer,
	STATE_ID smallint,
	TYPE_ID smallint,
	NAME varchar(48),
	AREA float,
	POPULATION integer,
	POPULATION_MALE integer,
	POPULATION_FEMALE integer,
	POPULATION_DENSITY integer,
	PRIMARY KEY (ID),
	FOREIGN KEY (STATE_ID) REFERENCES SOR_DIM_STATE (ID),
	FOREIGN KEY (TYPE_ID) REFERENCES SOR_DIM_COUNTY_TYPES (ID) 
);

-- source: https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/04-kreise.html;
INSERT INTO sor_dim_county VALUES (1001, 1, 0, 'Flensburg, Stadt', 56.73, 89504, 44599, 44905, 1578);
INSERT INTO sor_dim_county VALUES (1002, 1, 0, 'Kiel, Landeshauptstadt', 118.65, 247548, 120566, 126982, 2086);
INSERT INTO sor_dim_county VALUES (1003, 1, 0, 'Lübeck, Hansestadt', 214.19, 217198, 104371, 112827, 1014);
INSERT INTO sor_dim_county VALUES (1004, 1, 0, 'Neumünster, Stadt', 71.66, 79487, 39241, 40246, 1109);
INSERT INTO sor_dim_county VALUES (1051, 1, 1, 'Dithmarschen', 1428.17, 133210, 65720, 67490, 93);
INSERT INTO sor_dim_county VALUES (1053, 1, 1, 'Herzogtum Lauenburg', 1263.07, 197264, 96881, 100383, 156);
INSERT INTO sor_dim_county VALUES (1054, 1, 1, 'Nordfriesland', 2083.55, 165507, 81099, 84408, 79);
INSERT INTO sor_dim_county VALUES (1055, 1, 1, 'Ostholstein', 1393.02, 200581, 96765, 103816, 144);
INSERT INTO sor_dim_county VALUES (1056, 1, 1, 'Pinneberg', 664.25, 314391, 154211, 160180, 473);
INSERT INTO sor_dim_county VALUES (1057, 1, 1, 'Plön', 1083.56, 128647, 62532, 66115, 119);
INSERT INTO sor_dim_county VALUES (1058, 1, 1, 'Rendsburg-Eckernförde', 2189.79, 272775, 134109, 138666, 125);
INSERT INTO sor_dim_county VALUES (1059, 1, 1, 'Schleswig-Flensburg', 2071.28, 200025, 99092, 100933, 97);
INSERT INTO sor_dim_county VALUES (1060, 1, 1, 'Segeberg', 1344.47, 276032, 136517, 139515, 205);
INSERT INTO sor_dim_county VALUES (1061, 1, 1, 'Steinburg', 1055.7, 131347, 64923, 66424, 124);
INSERT INTO sor_dim_county VALUES (1062, 1, 1, 'Stormarn', 766.21, 243196, 118831, 124365, 317);
INSERT INTO sor_dim_county VALUES (2000, 2, 0, 'Hamburg, Freie und Hansestadt', 755.09, 1841179, 902048, 939131, 2438);
INSERT INTO sor_dim_county VALUES (3101, 3, 0, 'Braunschweig, Stadt', 192.7, 248292, 122985, 125307, 1288);
INSERT INTO sor_dim_county VALUES (3102, 3, 0, 'Salzgitter, Stadt', 224.49, 104948, 52202, 52746, 467);
INSERT INTO sor_dim_county VALUES (3103, 3, 0, 'Wolfsburg, Stadt', 204.61, 124151, 61820, 62331, 607);
INSERT INTO sor_dim_county VALUES (3151, 3, 2, 'Gifhorn', 1567.45, 175920, 88035, 87885, 112);
INSERT INTO sor_dim_county VALUES (3153, 3, 2, 'Goslar', 966.72, 137014, 67834, 69180, 142);
INSERT INTO sor_dim_county VALUES (3154, 3, 2, 'Helmstedt', 676.15, 91307, 45241, 46066, 135);
INSERT INTO sor_dim_county VALUES (3155, 3, 2, 'Northeim', 1268.76, 132765, 65445, 67320, 105);
INSERT INTO sor_dim_county VALUES (3157, 3, 2, 'Peine', 536.5, 133965, 66286, 67679, 250);
INSERT INTO sor_dim_county VALUES (3158, 3, 2, 'Wolfenbüttel', 724.3, 119960, 59370, 60590, 166);
INSERT INTO sor_dim_county VALUES (3159, 3, 2, 'Göttingen', 1755.41, 328074, 161187, 166887, 187);
INSERT INTO sor_dim_county VALUES (3241, 3, 2, 'Region Hannover', 2297.13, 1157624, 567201, 590423, 504);
INSERT INTO sor_dim_county VALUES (3251, 3, 2, 'Diepholz', 1991.01, 216886, 107853, 109033, 109);
INSERT INTO sor_dim_county VALUES (3252, 3, 2, 'Hameln-Pyrmont', 797.54, 148559, 71840, 76719, 186);
INSERT INTO sor_dim_county VALUES (3254, 3, 2, 'Hildesheim', 1208.33, 276594, 134912, 141682, 229);
INSERT INTO sor_dim_county VALUES (3255, 3, 2, 'Holzminden', 694.27, 70975, 35187, 35788, 102);
INSERT INTO sor_dim_county VALUES (3256, 3, 2, 'Nienburg (Weser)', 1400.82, 121386, 60400, 60986, 87);
INSERT INTO sor_dim_county VALUES (3257, 3, 2, 'Schaumburg', 675.65, 157781, 77174, 80607, 234);
INSERT INTO sor_dim_county VALUES (3351, 3, 2, 'Celle', 1550.82, 178936, 88054, 90882, 115);
INSERT INTO sor_dim_county VALUES (3352, 3, 2, 'Cuxhaven', 2058.96, 198213, 97044, 101169, 96);
INSERT INTO sor_dim_county VALUES (3353, 3, 2, 'Harburg', 1248.45, 252776, 124502, 128274, 202);
INSERT INTO sor_dim_county VALUES (3354, 3, 2, 'Lüchow-Dannenberg', 1227.16, 48424, 23839, 24585, 39);
INSERT INTO sor_dim_county VALUES (3355, 3, 2, 'Lüneburg', 1327.8, 183372, 89543, 93829, 138);
INSERT INTO sor_dim_county VALUES (3356, 3, 2, 'Osterholz', 652.67, 113517, 55765, 57752, 174);
INSERT INTO sor_dim_county VALUES (3357, 3, 2, 'Rotenburg (Wümme)', 2074.78, 163455, 82132, 81323, 79);
INSERT INTO sor_dim_county VALUES (3358, 3, 2, 'Heidekreis', 1881.45, 139755, 69757, 69998, 74);
INSERT INTO sor_dim_county VALUES (3359, 3, 2, 'Stade', 1267.38, 203102, 101043, 102059, 160);
INSERT INTO sor_dim_county VALUES (3360, 3, 2, 'Uelzen', 1462.59, 92572, 45241, 47331, 63);
INSERT INTO sor_dim_county VALUES (3361, 3, 2, 'Verden', 789.33, 136792, 67166, 69626, 173);
INSERT INTO sor_dim_county VALUES (3401, 3, 0, 'Delmenhorst, Stadt', 62.45, 77607, 38437, 39170, 1243);
INSERT INTO sor_dim_county VALUES (3402, 3, 0, 'Emden, Stadt', 112.34, 50195, 25084, 25111, 447);
INSERT INTO sor_dim_county VALUES (3403, 3, 0, 'Oldenburg (Oldenburg), Stadt', 103.09, 168210, 80501, 87709, 1632);
INSERT INTO sor_dim_county VALUES (3404, 3, 0, 'Osnabrück, Stadt', 119.8, 164748, 79756, 84992, 1375);
INSERT INTO sor_dim_county VALUES (3405, 3, 0, 'Wilhelmshaven, Stadt', 107.07, 76278, 37596, 38682, 712);
INSERT INTO sor_dim_county VALUES (3451, 3, 2, 'Ammerland', 730.64, 124071, 60658, 63413, 170);
INSERT INTO sor_dim_county VALUES (3452, 3, 2, 'Aurich', 1287.35, 189848, 93555, 96293, 147);
INSERT INTO sor_dim_county VALUES (3453, 3, 2, 'Cloppenburg', 1420.35, 169348, 85707, 83641, 119);
INSERT INTO sor_dim_county VALUES (3454, 3, 2, 'Emsland', 2883.66, 325657, 165784, 159873, 113);
INSERT INTO sor_dim_county VALUES (3455, 3, 2, 'Friesland', 609.53, 98460, 48064, 50396, 162);
INSERT INTO sor_dim_county VALUES (3456, 3, 2, 'Grafschaft Bentheim', 981.79, 136511, 68305, 68206, 139);
INSERT INTO sor_dim_county VALUES (3457, 3, 2, 'Leer', 1085.72, 169809, 84810, 84999, 156);
INSERT INTO sor_dim_county VALUES (3458, 3, 2, 'Oldenburg', 1064.84, 130144, 64574, 65570, 122);
INSERT INTO sor_dim_county VALUES (3459, 3, 2, 'Osnabrück', 2121.81, 357343, 177430, 179913, 168);
INSERT INTO sor_dim_county VALUES (3460, 3, 2, 'Vechta', 814.2, 141598, 71595, 70003, 174);
INSERT INTO sor_dim_county VALUES (3461, 3, 2, 'Wesermarsch', 824.78, 88624, 44410, 44214, 107);
INSERT INTO sor_dim_county VALUES (3462, 3, 2, 'Wittmund', 656.86, 56882, 27919, 28963, 87);
INSERT INTO sor_dim_county VALUES (4011, 4, 0, 'Bremen, Stadt', 317.83, 569352, 281336, 288016, 1791);
INSERT INTO sor_dim_county VALUES (4012, 4, 0, 'Bremerhaven, Stadt', 101.53, 113634, 56699, 56935, 1119);
INSERT INTO sor_dim_county VALUES (5111, 5, 0, 'Düsseldorf, Stadt', 217.41, 619294, 299315, 319979, 2849);
INSERT INTO sor_dim_county VALUES (5112, 5, 0, 'Duisburg, Stadt', 232.8, 498590, 246588, 252002, 2142);
INSERT INTO sor_dim_county VALUES (5113, 5, 0, 'Essen, Stadt', 210.34, 583109, 283005, 300104, 2772);
INSERT INTO sor_dim_county VALUES (5114, 5, 0, 'Krefeld, Stadt', 137.77, 227020, 110745, 116275, 1648);
INSERT INTO sor_dim_county VALUES (5116, 5, 0, 'Mönchengladbach, Stadt', 170.47, 261454, 128655, 132799, 1534);
INSERT INTO sor_dim_county VALUES (5117, 5, 0, 'Mülheim an der Ruhr, Stadt', 91.28, 170880, 82387, 88493, 1872);
INSERT INTO sor_dim_county VALUES (5119, 5, 0, 'Oberhausen, Stadt', 77.09, 210829, 103612, 107217, 2735);
INSERT INTO sor_dim_county VALUES (5120, 5, 0, 'Remscheid, Stadt', 74.52, 110994, 54664, 56330, 1489);
INSERT INTO sor_dim_county VALUES (5122, 5, 0, 'Solingen, Klingenstadt', 89.54, 159360, 77471, 81889, 1780);
INSERT INTO sor_dim_county VALUES (5124, 5, 0, 'Wuppertal, Stadt', 168.39, 354382, 173687, 180695, 2105);
INSERT INTO sor_dim_county VALUES (5154, 5, 1, 'Kleve', 1232.99, 310974, 154628, 156346, 252);
INSERT INTO sor_dim_county VALUES (5158, 5, 1, 'Mettmann', 407.22, 485684, 234705, 250979, 1193);
INSERT INTO sor_dim_county VALUES (5162, 5, 1, 'Rhein-Kreis Neuss', 576.28, 451007, 219214, 231793, 783);
INSERT INTO sor_dim_county VALUES (5166, 5, 1, 'Viersen', 563.28, 298935, 145913, 153022, 531);
INSERT INTO sor_dim_county VALUES (5170, 5, 1, 'Wesel', 1042.8, 459809, 224137, 235672, 441);
INSERT INTO sor_dim_county VALUES (5314, 5, 0, 'Bonn, Stadt', 141.06, 327258, 156328, 170930, 2320);
INSERT INTO sor_dim_county VALUES (5315, 5, 0, 'Köln, Stadt', 405.01, 1085664, 529368, 556296, 2681);
INSERT INTO sor_dim_county VALUES (5316, 5, 0, 'Leverkusen, Stadt', 78.87, 163838, 79787, 84051, 2077);
INSERT INTO sor_dim_county VALUES (5334, 5, 1, 'Städteregion Aachen', 706.91, 555465, 280162, 275303, 786);
INSERT INTO sor_dim_county VALUES (5358, 5, 1, 'Düren', 941.49, 263722, 130761, 132961, 280);
INSERT INTO sor_dim_county VALUES (5362, 5, 1, 'Rhein-Erft-Kreis', 704.71, 470089, 230062, 240027, 667);
INSERT INTO sor_dim_county VALUES (5366, 5, 1, 'Euskirchen', 1248.73, 192840, 95124, 97716, 154);
INSERT INTO sor_dim_county VALUES (5370, 5, 1, 'Heinsberg', 627.91, 254322, 125849, 128473, 405);
INSERT INTO sor_dim_county VALUES (5374, 5, 1, 'Oberbergischer Kreis', 918.85, 272471, 134069, 138402, 297);
INSERT INTO sor_dim_county VALUES (5378, 5, 1, 'Rheinisch-Bergischer Kreis', 437.32, 283455, 137572, 145883, 648);
INSERT INTO sor_dim_county VALUES (5382, 5, 1, 'Rhein-Sieg-Kreis', 1153.2, 599780, 294093, 305687, 520);
INSERT INTO sor_dim_county VALUES (5512, 5, 0, 'Bottrop, Stadt', 100.61, 117383, 57101, 60282, 1167);
INSERT INTO sor_dim_county VALUES (5513, 5, 0, 'Gelsenkirchen, Stadt', 104.94, 260654, 129778, 130876, 2484);
INSERT INTO sor_dim_county VALUES (5515, 5, 0, 'Münster, Stadt', 303.28, 314319, 150836, 163483, 1036);
INSERT INTO sor_dim_county VALUES (5554, 5, 1, 'Borken', 1420.98, 370676, 185195, 185481, 261);
INSERT INTO sor_dim_county VALUES (5558, 5, 1, 'Coesfeld', 1112.05, 219929, 108564, 111365, 198);
INSERT INTO sor_dim_county VALUES (5562, 5, 1, 'Recklinghausen', 761.31, 615261, 300019, 315242, 808);
INSERT INTO sor_dim_county VALUES (5566, 5, 1, 'Steinfurt', 1795.76, 447614, 222633, 224981, 249);
INSERT INTO sor_dim_county VALUES (5570, 5, 1, 'Warendorf', 1319.42, 277783, 137298, 140485, 211);
INSERT INTO sor_dim_county VALUES (5711, 5, 0, 'Bielefeld, Stadt', 258.83, 333786, 161078, 172708, 1290);
INSERT INTO sor_dim_county VALUES (5754, 5, 1, 'Gütersloh', 969.21, 364083, 181844, 182239, 376);
INSERT INTO sor_dim_county VALUES (5758, 5, 1, 'Herford', 450.41, 250783, 122959, 127824, 557);
INSERT INTO sor_dim_county VALUES (5762, 5, 1, 'Höxter', 1201.42, 140667, 70059, 70608, 117);
INSERT INTO sor_dim_county VALUES (5766, 5, 1, 'Lippe', 1246.22, 348391, 170965, 177426, 280);
INSERT INTO sor_dim_county VALUES (5770, 5, 1, 'Minden-Lübbecke', 1152.41, 310710, 153305, 157405, 270);
INSERT INTO sor_dim_county VALUES (5774, 5, 1, 'Paderborn', 1246.8, 306890, 153622, 153268, 246);
INSERT INTO sor_dim_county VALUES (5911, 5, 0, 'Bochum, Stadt', 145.66, 364628, 177699, 186929, 2503);
INSERT INTO sor_dim_county VALUES (5913, 5, 0, 'Dortmund, Stadt', 280.71, 587010, 288386, 298624, 2091);
INSERT INTO sor_dim_county VALUES (5914, 5, 0, 'Hagen, Stadt der FernUniversität', 160.45, 188814, 92168, 96646, 1177);
INSERT INTO sor_dim_county VALUES (5915, 5, 0, 'Hamm, Stadt', 226.43, 179111, 88024, 91087, 791);
INSERT INTO sor_dim_county VALUES (5916, 5, 0, 'Herne, Stadt', 51.42, 156374, 76649, 79725, 3041);
INSERT INTO sor_dim_county VALUES (5954, 5, 1, 'Ennepe-Ruhr-Kreis', 409.64, 324296, 157490, 166806, 792);
INSERT INTO sor_dim_county VALUES (5958, 5, 1, 'Hochsauerlandkreis', 1960.17, 260475, 129947, 130528, 133);
INSERT INTO sor_dim_county VALUES (5962, 5, 1, 'Märkischer Kreis', 1061.06, 412120, 202817, 209303, 388);
INSERT INTO sor_dim_county VALUES (5966, 5, 1, 'Olpe', 712.14, 134775, 67610, 67165, 189);
INSERT INTO sor_dim_county VALUES (5970, 5, 1, 'Siegen-Wittgenstein', 1132.89, 278210, 138358, 139852, 246);
INSERT INTO sor_dim_county VALUES (5974, 5, 1, 'Soest', 1328.64, 301902, 150022, 151880, 227);
INSERT INTO sor_dim_county VALUES (5978, 5, 1, 'Unna', 543.21, 394782, 192304, 202478, 727);
INSERT INTO sor_dim_county VALUES (6411, 6, 0, 'Darmstadt, Wissenschaftsstadt', 122.07, 159207, 81217, 77990, 1304);
INSERT INTO sor_dim_county VALUES (6412, 6, 0, 'Frankfurt am Main, Stadt', 248.31, 753056, 371671, 381385, 3033);
INSERT INTO sor_dim_county VALUES (6413, 6, 0, 'Offenbach am Main, Stadt', 44.88, 128744, 63699, 65045, 2869);
INSERT INTO sor_dim_county VALUES (6414, 6, 0, 'Wiesbaden, Landeshauptstadt', 203.87, 278342, 132943, 145399, 1365);
INSERT INTO sor_dim_county VALUES (6431, 6, 2, 'Bergstraße', 719.47, 269694, 133318, 136376, 375);
INSERT INTO sor_dim_county VALUES (6432, 6, 2, 'Darmstadt-Dieburg', 658.64, 297399, 147786, 149613, 452);
INSERT INTO sor_dim_county VALUES (6433, 6, 2, 'Groß-Gerau', 453.03, 274526, 137325, 137201, 606);
INSERT INTO sor_dim_county VALUES (6434, 6, 2, 'Hochtaunuskreis', 481.84, 236564, 114878, 121686, 491);
INSERT INTO sor_dim_county VALUES (6435, 6, 2, 'Main-Kinzig-Kreis', 1397.32, 418950, 206555, 212395, 300);
INSERT INTO sor_dim_county VALUES (6436, 6, 2, 'Main-Taunus-Kreis', 222.53, 237735, 116332, 121403, 1068);
INSERT INTO sor_dim_county VALUES (6437, 6, 2, 'Odenwaldkreis', 623.97, 96798, 47875, 48923, 155);
INSERT INTO sor_dim_county VALUES (6438, 6, 2, 'Offenbach', 356.24, 354092, 174417, 179675, 994);
INSERT INTO sor_dim_county VALUES (6439, 6, 2, 'Rheingau-Taunus-Kreis', 811.41, 187157, 91962, 95195, 231);
INSERT INTO sor_dim_county VALUES (6440, 6, 2, 'Wetteraukreis', 1100.66, 306460, 151276, 155184, 278);
INSERT INTO sor_dim_county VALUES (6531, 6, 2, 'Gießen', 854.56, 268876, 132135, 136741, 315);
INSERT INTO sor_dim_county VALUES (6532, 6, 2, 'Lahn-Dill-Kreis', 1066.3, 253777, 125090, 128687, 238);
INSERT INTO sor_dim_county VALUES (6533, 6, 2, 'Limburg-Weilburg', 738.44, 172083, 85571, 86512, 233);
INSERT INTO sor_dim_county VALUES (6534, 6, 2, 'Marburg-Biedenkopf', 1262.37, 246648, 121414, 125234, 195);
INSERT INTO sor_dim_county VALUES (6535, 6, 2, 'Vogelsbergkreis', 1458.91, 105878, 52992, 52886, 73);
INSERT INTO sor_dim_county VALUES (6611, 6, 0, 'Kassel, documenta-Stadt', 106.8, 201585, 99007, 102578, 1888);
INSERT INTO sor_dim_county VALUES (6631, 6, 2, 'Fulda', 1380.41, 222584, 110592, 111992, 161);
INSERT INTO sor_dim_county VALUES (6632, 6, 2, 'Hersfeld-Rotenburg', 1097.75, 120829, 60523, 60306, 110);
INSERT INTO sor_dim_county VALUES (6633, 6, 2, 'Kassel', 1293.35, 236633, 116122, 120511, 183);
INSERT INTO sor_dim_county VALUES (6634, 6, 2, 'Schwalm-Eder-Kreis', 1539.01, 180222, 90181, 90041, 117);
INSERT INTO sor_dim_county VALUES (6635, 6, 2, 'Waldeck-Frankenberg', 1848.7, 156953, 78198, 78755, 85);
INSERT INTO sor_dim_county VALUES (6636, 6, 2, 'Werra-Meißner-Kreis', 1024.83, 101017, 49965, 51052, 99);
INSERT INTO sor_dim_county VALUES (7111, 7, 0, 'Koblenz, kreisfreie Stadt', 105.25, 114024, 55628, 58396, 1083);
INSERT INTO sor_dim_county VALUES (7131, 7, 2, 'Ahrweiler', 787.02, 129727, 63755, 65972, 165);
INSERT INTO sor_dim_county VALUES (7132, 7, 2, 'Altenkirchen (Westerwald)', 642.38, 128705, 63859, 64846, 200);
INSERT INTO sor_dim_county VALUES (7133, 7, 2, 'Bad Kreuznach', 863.89, 158080, 77185, 80895, 183);
INSERT INTO sor_dim_county VALUES (7134, 7, 2, 'Birkenfeld', 776.83, 80720, 40021, 40699, 104);
INSERT INTO sor_dim_county VALUES (7135, 7, 2, 'Cochem-Zell', 692.43, 61587, 30841, 30746, 89);
INSERT INTO sor_dim_county VALUES (7137, 7, 2, 'Mayen-Koblenz', 817.73, 214259, 105746, 108513, 262);
INSERT INTO sor_dim_county VALUES (7138, 7, 2, 'Neuwied', 627.06, 181941, 89488, 92453, 290);
INSERT INTO sor_dim_county VALUES (7140, 7, 2, 'Rhein-Hunsrück-Kreis', 991.06, 102937, 51206, 51731, 104);
INSERT INTO sor_dim_county VALUES (7141, 7, 2, 'Rhein-Lahn-Kreis', 782.24, 122308, 60525, 61783, 156);
INSERT INTO sor_dim_county VALUES (7143, 7, 2, 'Westerwaldkreis', 989.04, 201597, 100218, 101379, 204);
INSERT INTO sor_dim_county VALUES (7211, 7, 0, 'Trier, kreisfreie Stadt', 117.06, 110636, 54444, 56192, 945);
INSERT INTO sor_dim_county VALUES (7231, 7, 2, 'Bernkastel-Wittlich', 1167.92, 112262, 56124, 56138, 96);
INSERT INTO sor_dim_county VALUES (7232, 7, 2, 'Eifelkreis Bitburg-Prüm', 1626.95, 98561, 49576, 48985, 61);
INSERT INTO sor_dim_county VALUES (7233, 7, 2, 'Vulkaneifel', 911.64, 60603, 30427, 30176, 66);
INSERT INTO sor_dim_county VALUES (7235, 7, 2, 'Trier-Saarburg', 1102.26, 148945, 73673, 75272, 135);
INSERT INTO sor_dim_county VALUES (7311, 7, 0, 'Frankenthal (Pfalz), kreisfreie Stadt', 43.88, 48561, 23489, 25072, 1107);
INSERT INTO sor_dim_county VALUES (7312, 7, 0, 'Kaiserslautern, kreisfreie Stadt', 139.7, 99845, 50353, 49492, 715);
INSERT INTO sor_dim_county VALUES (7313, 7, 0, 'Landau in der Pfalz, kreisfreie Stadt', 82.94, 46677, 22078, 24599, 563);
INSERT INTO sor_dim_county VALUES (7314, 7, 0, 'Ludwigshafen am Rhein, kreisfreie Stadt', 77.43, 171061, 85820, 85241, 2209);
INSERT INTO sor_dim_county VALUES (7315, 7, 0, 'Mainz, kreisfreie Stadt', 97.73, 217118, 105599, 111519, 2222);
INSERT INTO sor_dim_county VALUES (7316, 7, 0, 'Neustadt an der Weinstraße, kreisfreie Stadt', 117.09, 53148, 25955, 27193, 454);
INSERT INTO sor_dim_county VALUES (7317, 7, 0, 'Pirmasens, kreisfreie Stadt', 61.36, 40403, 19780, 20623, 658);
INSERT INTO sor_dim_county VALUES (7318, 7, 0, 'Speyer, kreisfreie Stadt', 42.71, 50378, 24363, 26015, 1180);
INSERT INTO sor_dim_county VALUES (7319, 7, 0, 'Worms, kreisfreie Stadt', 108.73, 83330, 41080, 42250, 766);
INSERT INTO sor_dim_county VALUES (7320, 7, 0, 'Zweibrücken, kreisfreie Stadt', 70.64, 34209, 16954, 17255, 484);
INSERT INTO sor_dim_county VALUES (7331, 7, 2, 'Alzey-Worms', 588.07, 129244, 64389, 64855, 220);
INSERT INTO sor_dim_county VALUES (7332, 7, 2, 'Bad Dürkheim', 594.64, 132660, 64838, 67822, 223);
INSERT INTO sor_dim_county VALUES (7333, 7, 2, 'Donnersbergkreis', 645.41, 75101, 37194, 37907, 116);
INSERT INTO sor_dim_county VALUES (7334, 7, 2, 'Germersheim', 463.32, 129075, 64379, 64696, 279);
INSERT INTO sor_dim_county VALUES (7335, 7, 2, 'Kaiserslautern', 640, 106057, 52135, 53922, 166);
INSERT INTO sor_dim_county VALUES (7336, 7, 2, 'Kusel', 573.61, 70526, 35052, 35474, 123);
INSERT INTO sor_dim_county VALUES (7337, 7, 2, 'Südliche Weinstraße', 639.93, 110356, 54441, 55915, 172);
INSERT INTO sor_dim_county VALUES (7338, 7, 2, 'Rhein-Pfalz-Kreis', 304.99, 154201, 76030, 78171, 506);
INSERT INTO sor_dim_county VALUES (7339, 7, 2, 'Mainz-Bingen', 605.36, 210889, 104015, 106874, 348);
INSERT INTO sor_dim_county VALUES (7340, 7, 2, 'Südwestpfalz', 953.52, 95113, 46916, 48197, 100);
INSERT INTO sor_dim_county VALUES (8111, 8, 3, 'Stuttgart, Stadtkreis', 207.33, 634830, 317139, 317691, 3062);
INSERT INTO sor_dim_county VALUES (8115, 8, 2, 'Böblingen', 617.76, 391640, 194989, 196651, 634);
INSERT INTO sor_dim_county VALUES (8116, 8, 2, 'Esslingen', 641.28, 533859, 266783, 267076, 832);
INSERT INTO sor_dim_county VALUES (8117, 8, 2, 'Göppingen', 642.34, 257253, 128093, 129160, 400);
INSERT INTO sor_dim_county VALUES (8118, 8, 2, 'Ludwigsburg', 686.77, 543984, 269961, 274023, 792);
INSERT INTO sor_dim_county VALUES (8119, 8, 2, 'Rems-Murr-Kreis', 858.08, 426158, 210861, 215297, 497);
INSERT INTO sor_dim_county VALUES (8121, 8, 3, 'Heilbronn, Stadtkreis', 99.89, 125960, 63527, 62433, 1261);
INSERT INTO sor_dim_county VALUES (8125, 8, 2, 'Heilbronn', 1099.91, 343068, 172382, 170686, 312);
INSERT INTO sor_dim_county VALUES (8126, 8, 2, 'Hohenlohekreis', 776.76, 112010, 56550, 55460, 144);
INSERT INTO sor_dim_county VALUES (8127, 8, 2, 'Schwäbisch Hall', 1484.07, 195861, 98314, 97547, 132);
INSERT INTO sor_dim_county VALUES (8128, 8, 2, 'Main-Tauber-Kreis', 1304.12, 132321, 65923, 66398, 101);
INSERT INTO sor_dim_county VALUES (8135, 8, 2, 'Heidenheim', 627.13, 132472, 66092, 66380, 211);
INSERT INTO sor_dim_county VALUES (8136, 8, 2, 'Ostalbkreis', 1511.39, 314002, 156480, 157522, 208);
INSERT INTO sor_dim_county VALUES (8211, 8, 3, 'Baden-Baden, Stadtkreis', 140.19, 55123, 26442, 28681, 393);
INSERT INTO sor_dim_county VALUES (8212, 8, 3, 'Karlsruhe, Stadtkreis', 173.42, 313092, 160429, 152663, 1805);
INSERT INTO sor_dim_county VALUES (8215, 8, 2, 'Karlsruhe', 1085.29, 444232, 221339, 222893, 409);
INSERT INTO sor_dim_county VALUES (8216, 8, 2, 'Rastatt', 738.43, 231018, 114797, 116221, 313);
INSERT INTO sor_dim_county VALUES (8221, 8, 3, 'Heidelberg, Stadtkreis', 108.83, 160355, 77112, 83243, 1473);
INSERT INTO sor_dim_county VALUES (8222, 8, 3, 'Mannheim, Stadtkreis', 144.97, 309370, 154273, 155097, 2134);
INSERT INTO sor_dim_county VALUES (8225, 8, 2, 'Neckar-Odenwald-Kreis', 1125.95, 143535, 72057, 71478, 127);
INSERT INTO sor_dim_county VALUES (8226, 8, 2, 'Rhein-Neckar-Kreis', 1061.55, 547625, 269317, 278308, 516);
INSERT INTO sor_dim_county VALUES (8231, 8, 3, 'Pforzheim, Stadtkreis', 97.99, 125542, 61699, 63843, 1281);
INSERT INTO sor_dim_county VALUES (8235, 8, 2, 'Calw', 797.29, 158397, 79514, 78883, 199);
INSERT INTO sor_dim_county VALUES (8236, 8, 2, 'Enzkreis', 573.6, 198905, 98744, 100161, 347);
INSERT INTO sor_dim_county VALUES (8237, 8, 2, 'Freudenstadt', 870.4, 117935, 58898, 59037, 135);
INSERT INTO sor_dim_county VALUES (8311, 8, 3, 'Freiburg im Breisgau, Stadtkreis', 153.04, 230241, 109829, 120412, 1504);
INSERT INTO sor_dim_county VALUES (8315, 8, 2, 'Breisgau-Hochschwarzwald', 1378.32, 262795, 129641, 133154, 191);
INSERT INTO sor_dim_county VALUES (8316, 8, 2, 'Emmendingen', 679.8, 165383, 81772, 83611, 243);
INSERT INTO sor_dim_county VALUES (8317, 8, 2, 'Ortenaukreis', 1860.29, 429479, 214221, 215258, 231);
INSERT INTO sor_dim_county VALUES (8325, 8, 2, 'Rottweil', 769.42, 139455, 69767, 69688, 181);
INSERT INTO sor_dim_county VALUES (8326, 8, 2, 'Schwarzwald-Baar-Kreis', 1025.34, 212381, 105542, 106839, 207);
INSERT INTO sor_dim_county VALUES (8327, 8, 2, 'Tuttlingen', 734.38, 140152, 70452, 69700, 191);
INSERT INTO sor_dim_county VALUES (8335, 8, 2, 'Konstanz', 817.98, 285325, 139046, 146279, 349);
INSERT INTO sor_dim_county VALUES (8336, 8, 2, 'Lörrach', 806.72, 228639, 111981, 116658, 283);
INSERT INTO sor_dim_county VALUES (8337, 8, 2, 'Waldshut', 1131.14, 170619, 84587, 86032, 151);
INSERT INTO sor_dim_county VALUES (8415, 8, 2, 'Reutlingen', 1092.48, 286748, 142232, 144516, 262);
INSERT INTO sor_dim_county VALUES (8416, 8, 2, 'Tübingen', 519.11, 227331, 110795, 116536, 438);
INSERT INTO sor_dim_county VALUES (8417, 8, 2, 'Zollernalbkreis', 917.58, 188935, 93808, 95127, 206);
INSERT INTO sor_dim_county VALUES (8421, 8, 3, 'Ulm, Stadtkreis', 118.68, 126329, 62660, 63669, 1064);
INSERT INTO sor_dim_county VALUES (8425, 8, 2, 'Alb-Donau-Kreis', 1358.55, 196047, 99041, 97006, 144);
INSERT INTO sor_dim_county VALUES (8426, 8, 2, 'Biberach', 1409.52, 199742, 100634, 99108, 142);
INSERT INTO sor_dim_county VALUES (8435, 8, 2, 'Bodenseekreis', 664.8, 216227, 106544, 109683, 325);
INSERT INTO sor_dim_county VALUES (8436, 8, 2, 'Ravensburg', 1632.08, 284285, 141777, 142508, 174);
INSERT INTO sor_dim_county VALUES (8437, 8, 2, 'Sigmaringen', 1204.23, 130873, 65649, 65224, 109);
INSERT INTO sor_dim_county VALUES (9161, 9, 0, 'Ingolstadt', 133.35, 136981, 69252, 67729, 1027);
INSERT INTO sor_dim_county VALUES (9162, 9, 0, 'München, Landeshauptstadt', 310.7, 1471508, 717308, 754200, 4736);
INSERT INTO sor_dim_county VALUES (9163, 9, 0, 'Rosenheim', 37.22, 63324, 31321, 32003, 1701);
INSERT INTO sor_dim_county VALUES (9171, 9, 2, 'Altötting', 569.28, 111210, 54942, 56268, 195);
INSERT INTO sor_dim_county VALUES (9172, 9, 2, 'Berchtesgadener Land', 839.82, 105722, 52007, 53715, 126);
INSERT INTO sor_dim_county VALUES (9173, 9, 2, 'Bad Tölz-Wolfratshausen', 1110.66, 127227, 62731, 64496, 115);
INSERT INTO sor_dim_county VALUES (9174, 9, 2, 'Dachau', 579.16, 153884, 77120, 76764, 266);
INSERT INTO sor_dim_county VALUES (9175, 9, 2, 'Ebersberg', 549.39, 142142, 70972, 71170, 259);
INSERT INTO sor_dim_county VALUES (9176, 9, 2, 'Eichstätt', 1213.85, 132341, 67325, 65016, 109);
INSERT INTO sor_dim_county VALUES (9177, 9, 2, 'Erding', 870.74, 137660, 69367, 68293, 158);
INSERT INTO sor_dim_county VALUES (9178, 9, 2, 'Freising', 799.85, 179116, 91393, 87723, 224);
INSERT INTO sor_dim_county VALUES (9179, 9, 2, 'Fürstenfeldbruck', 434.8, 219320, 107686, 111634, 504);
INSERT INTO sor_dim_county VALUES (9180, 9, 2, 'Garmisch-Partenkirchen', 1012.17, 88467, 42969, 45498, 87);
INSERT INTO sor_dim_county VALUES (9181, 9, 2, 'Landsberg am Lech', 804.36, 120071, 60077, 59994, 149);
INSERT INTO sor_dim_county VALUES (9182, 9, 2, 'Miesbach', 866.22, 99726, 48870, 50856, 115);
INSERT INTO sor_dim_county VALUES (9183, 9, 2, 'Mühldorf a.Inn', 805.33, 115250, 58360, 56890, 143);
INSERT INTO sor_dim_county VALUES (9184, 9, 2, 'München', 664.25, 348871, 173601, 175270, 525);
INSERT INTO sor_dim_county VALUES (9185, 9, 2, 'Neuburg-Schrobenhausen', 739.71, 96680, 48679, 48001, 131);
INSERT INTO sor_dim_county VALUES (9186, 9, 2, 'Pfaffenhofen a.d.Ilm', 761.05, 127151, 64639, 62512, 167);
INSERT INTO sor_dim_county VALUES (9187, 9, 2, 'Rosenheim', 1439.44, 260983, 129253, 131730, 181);
INSERT INTO sor_dim_county VALUES (9188, 9, 2, 'Starnberg', 487.72, 136092, 66020, 70072, 279);
INSERT INTO sor_dim_county VALUES (9189, 9, 2, 'Traunstein', 1533.76, 177089, 87564, 89525, 115);
INSERT INTO sor_dim_county VALUES (9190, 9, 2, 'Weilheim-Schongau', 966.28, 135348, 67321, 68027, 140);
INSERT INTO sor_dim_county VALUES (9261, 9, 0, 'Landshut', 65.83, 72404, 35403, 37001, 1100);
INSERT INTO sor_dim_county VALUES (9262, 9, 0, 'Passau', 69.56, 52469, 25344, 27125, 754);
INSERT INTO sor_dim_county VALUES (9263, 9, 0, 'Straubing', 67.59, 47794, 24117, 23677, 707);
INSERT INTO sor_dim_county VALUES (9271, 9, 2, 'Deggendorf', 861.17, 119326, 59547, 59779, 139);
INSERT INTO sor_dim_county VALUES (9272, 9, 2, 'Freyung-Grafenau', 983.85, 78355, 39103, 39252, 80);
INSERT INTO sor_dim_county VALUES (9273, 9, 2, 'Kelheim', 1065.13, 122258, 62137, 60121, 115);
INSERT INTO sor_dim_county VALUES (9274, 9, 2, 'Landshut', 1347.56, 158698, 80519, 78179, 118);
INSERT INTO sor_dim_county VALUES (9275, 9, 2, 'Passau', 1530.09, 192043, 95471, 96572, 126);
INSERT INTO sor_dim_county VALUES (9276, 9, 2, 'Regen', 974.78, 77656, 38745, 38911, 80);
INSERT INTO sor_dim_county VALUES (9277, 9, 2, 'Rottal-Inn', 1281.2, 120659, 60346, 60313, 94);
INSERT INTO sor_dim_county VALUES (9278, 9, 2, 'Straubing-Bogen', 1201.61, 100649, 50627, 50022, 84);
INSERT INTO sor_dim_county VALUES (9279, 9, 2, 'Dingolfing-Landau', 877.58, 96217, 49450, 46767, 110);
INSERT INTO sor_dim_county VALUES (9361, 9, 0, 'Amberg', 50.13, 41970, 20673, 21297, 837);
INSERT INTO sor_dim_county VALUES (9362, 9, 0, 'Regensburg', 80.86, 152610, 74225, 78385, 1887);
INSERT INTO sor_dim_county VALUES (9363, 9, 0, 'Weiden i.d.OPf.', 70.57, 42520, 20542, 21978, 603);
INSERT INTO sor_dim_county VALUES (9371, 9, 2, 'Amberg-Sulzbach', 1255.86, 103109, 51665, 51444, 82);
INSERT INTO sor_dim_county VALUES (9372, 9, 2, 'Cham', 1526.82, 127882, 64248, 63634, 84);
INSERT INTO sor_dim_county VALUES (9373, 9, 2, 'Neumarkt i.d.OPf.', 1343.96, 133561, 67499, 66062, 99);
INSERT INTO sor_dim_county VALUES (9374, 9, 2, 'Neustadt a.d.Waldnaab', 1427.69, 94352, 46943, 47409, 66);
INSERT INTO sor_dim_county VALUES (9375, 9, 2, 'Regensburg', 1391.65, 193572, 97081, 96491, 139);
INSERT INTO sor_dim_county VALUES (9376, 9, 2, 'Schwandorf', 1458.34, 147189, 73964, 73225, 101);
INSERT INTO sor_dim_county VALUES (9377, 9, 2, 'Tirschenreuth', 1084.25, 72504, 36121, 36383, 67);
INSERT INTO sor_dim_county VALUES (9461, 9, 0, 'Bamberg', 54.62, 77592, 37464, 40128, 1421);
INSERT INTO sor_dim_county VALUES (9462, 9, 0, 'Bayreuth', 66.89, 74657, 36469, 38188, 1116);
INSERT INTO sor_dim_county VALUES (9463, 9, 0, 'Coburg', 48.29, 41249, 20162, 21087, 854);
INSERT INTO sor_dim_county VALUES (9464, 9, 0, 'Hof', 58.02, 45930, 22098, 23832, 792);
INSERT INTO sor_dim_county VALUES (9471, 9, 2, 'Bamberg', 1167.79, 147086, 73725, 73361, 126);
INSERT INTO sor_dim_county VALUES (9472, 9, 2, 'Bayreuth', 1273.62, 103656, 51310, 52346, 81);
INSERT INTO sor_dim_county VALUES (9473, 9, 2, 'Coburg', 590.42, 86906, 42912, 43994, 147);
INSERT INTO sor_dim_county VALUES (9474, 9, 2, 'Forchheim', 642.82, 116099, 57703, 58396, 181);
INSERT INTO sor_dim_county VALUES (9475, 9, 2, 'Hof', 892.52, 95311, 46646, 48665, 107);
INSERT INTO sor_dim_county VALUES (9476, 9, 2, 'Kronach', 651.49, 67135, 33184, 33951, 103);
INSERT INTO sor_dim_county VALUES (9477, 9, 2, 'Kulmbach', 658.33, 71845, 35345, 36500, 109);
INSERT INTO sor_dim_county VALUES (9478, 9, 2, 'Lichtenfels', 519.94, 66838, 33027, 33811, 129);
INSERT INTO sor_dim_county VALUES (9479, 9, 2, 'Wunsiedel i.Fichtelgebirge', 606.37, 73178, 35718, 37460, 121);
INSERT INTO sor_dim_county VALUES (9561, 9, 0, 'Ansbach', 99.91, 41847, 20048, 21799, 419);
INSERT INTO sor_dim_county VALUES (9562, 9, 0, 'Erlangen', 76.96, 111962, 55864, 56098, 1455);
INSERT INTO sor_dim_county VALUES (9563, 9, 0, 'Fürth', 63.35, 127748, 62603, 65145, 2017);
INSERT INTO sor_dim_county VALUES (9564, 9, 0, 'Nürnberg', 186.45, 518365, 252807, 265558, 2780);
INSERT INTO sor_dim_county VALUES (9565, 9, 0, 'Schwabach', 40.8, 40792, 19895, 20897, 1000);
INSERT INTO sor_dim_county VALUES (9571, 9, 2, 'Ansbach', 1971.33, 183949, 92450, 91499, 93);
INSERT INTO sor_dim_county VALUES (9572, 9, 2, 'Erlangen-Höchstadt', 564.56, 136271, 67763, 68508, 241);
INSERT INTO sor_dim_county VALUES (9573, 9, 2, 'Fürth', 307.44, 117387, 57064, 60323, 382);
INSERT INTO sor_dim_county VALUES (9574, 9, 2, 'Nürnberger Land', 799.51, 170365, 83946, 86419, 213);
INSERT INTO sor_dim_county VALUES (9575, 9, 2, 'Neustadt a.d.Aisch-Bad Windsheim', 1267.44, 100364, 50331, 50033, 79);
INSERT INTO sor_dim_county VALUES (9576, 9, 2, 'Roth', 895.16, 126958, 63206, 63752, 142);
INSERT INTO sor_dim_county VALUES (9577, 9, 2, 'Weißenburg-Gunzenhausen', 970.78, 94393, 47093, 47300, 97);
INSERT INTO sor_dim_county VALUES (9661, 9, 0, 'Aschaffenburg', 62.45, 70527, 34316, 36211, 1129);
INSERT INTO sor_dim_county VALUES (9662, 9, 0, 'Schweinfurt', 35.7, 54032, 26553, 27479, 1514);
INSERT INTO sor_dim_county VALUES (9663, 9, 0, 'Würzburg', 87.6, 127880, 61151, 66729, 1460);
INSERT INTO sor_dim_county VALUES (9671, 9, 2, 'Aschaffenburg', 698.9, 174208, 86570, 87638, 249);
INSERT INTO sor_dim_county VALUES (9672, 9, 2, 'Bad Kissingen', 1136.9, 103218, 50865, 52353, 91);
INSERT INTO sor_dim_county VALUES (9673, 9, 2, 'Rhön-Grabfeld', 1021.68, 79690, 39677, 40013, 78);
INSERT INTO sor_dim_county VALUES (9674, 9, 2, 'Haßberge', 956.19, 84599, 42468, 42131, 88);
INSERT INTO sor_dim_county VALUES (9675, 9, 2, 'Kitzingen', 684.14, 90909, 45791, 45118, 133);
INSERT INTO sor_dim_county VALUES (9676, 9, 2, 'Miltenberg', 715.58, 128756, 64190, 64566, 180);
INSERT INTO sor_dim_county VALUES (9677, 9, 2, 'Main-Spessart', 1321.2, 126365, 62953, 63412, 96);
INSERT INTO sor_dim_county VALUES (9678, 9, 2, 'Schweinfurt', 841.39, 115106, 57768, 57338, 137);
INSERT INTO sor_dim_county VALUES (9679, 9, 2, 'Würzburg', 968.35, 161834, 80594, 81240, 167);
INSERT INTO sor_dim_county VALUES (9761, 9, 0, 'Augsburg', 146.85, 295135, 145158, 149977, 2010);
INSERT INTO sor_dim_county VALUES (9762, 9, 0, 'Kaufbeuren', 40.02, 43893, 21610, 22283, 1097);
INSERT INTO sor_dim_county VALUES (9763, 9, 0, 'Kempten (Allgäu)', 63.28, 68907, 34141, 34766, 1089);
INSERT INTO sor_dim_county VALUES (9764, 9, 0, 'Memmingen', 70.11, 43837, 21710, 22127, 625);
INSERT INTO sor_dim_county VALUES (9771, 9, 2, 'Aichach-Friedberg', 780.23, 133596, 66651, 66945, 171);
INSERT INTO sor_dim_county VALUES (9772, 9, 2, 'Augsburg', 1070.64, 251534, 124807, 126727, 235);
INSERT INTO sor_dim_county VALUES (9773, 9, 2, 'Dillingen a.d.Donau', 792.23, 96021, 48135, 47886, 121);
INSERT INTO sor_dim_county VALUES (9774, 9, 2, 'Günzburg', 762.4, 125747, 63576, 62171, 165);
INSERT INTO sor_dim_county VALUES (9775, 9, 2, 'Neu-Ulm', 515.84, 174200, 86470, 87730, 338);
INSERT INTO sor_dim_county VALUES (9776, 9, 2, 'Lindau (Bodensee)', 323.39, 81669, 40285, 41384, 253);
INSERT INTO sor_dim_county VALUES (9777, 9, 2, 'Ostallgäu', 1394.43, 140316, 69968, 70348, 101);
INSERT INTO sor_dim_county VALUES (9778, 9, 2, 'Unterallgäu', 1229.57, 144041, 72608, 71433, 117);
INSERT INTO sor_dim_county VALUES (9779, 9, 2, 'Donau-Ries', 1274.58, 133496, 67821, 65675, 105);
INSERT INTO sor_dim_county VALUES (9780, 9, 2, 'Oberallgäu', 1527.96, 155362, 76577, 78785, 102);
INSERT INTO sor_dim_county VALUES (10041, 10, 4, 'Regionalverband Saarbrücken', 410.95, 329708, 162003, 167705, 802);
INSERT INTO sor_dim_county VALUES (10042, 10, 2, 'Merzig-Wadern', 556.66, 103366, 51108, 52258, 186);
INSERT INTO sor_dim_county VALUES (10043, 10, 2, 'Neunkirchen', 249.8, 132206, 64915, 67291, 529);
INSERT INTO sor_dim_county VALUES (10044, 10, 2, 'Saarlouis', 459.35, 195201, 95550, 99651, 425);
INSERT INTO sor_dim_county VALUES (10045, 10, 2, 'Saarpfalz-Kreis', 418.28, 142631, 69385, 73246, 341);
INSERT INTO sor_dim_county VALUES (10046, 10, 2, 'St. Wendel', 476.07, 87397, 43198, 44199, 184);
INSERT INTO sor_dim_county VALUES (11000, 11, 0, 'Berlin, Stadt', 891.12, 3644826, 1792801, 1852025, 4090);
INSERT INTO sor_dim_county VALUES (12051, 12, 0, 'Brandenburg an der Havel, Stadt', 229.71, 72124, 35617, 36507, 314);
INSERT INTO sor_dim_county VALUES (12052, 12, 0, 'Cottbus, Stadt', 165.63, 100219, 49201, 51018, 605);
INSERT INTO sor_dim_county VALUES (12053, 12, 0, 'Frankfurt (Oder), Stadt', 147.85, 57873, 28023, 29850, 391);
INSERT INTO sor_dim_county VALUES (12054, 12, 0, 'Potsdam, Stadt', 188.25, 178089, 86179, 91910, 946);
INSERT INTO sor_dim_county VALUES (12060, 12, 2, 'Barnim', 1479.61, 182760, 90615, 92145, 124);
INSERT INTO sor_dim_county VALUES (12061, 12, 2, 'Dahme-Spreewald', 2274.53, 169067, 83943, 85124, 74);
INSERT INTO sor_dim_county VALUES (12062, 12, 2, 'Elbe-Elster', 1899.16, 102638, 50832, 51806, 54);
INSERT INTO sor_dim_county VALUES (12063, 12, 2, 'Havelland', 1727.31, 161909, 80121, 81788, 94);
INSERT INTO sor_dim_county VALUES (12064, 12, 2, 'Märkisch-Oderland', 2158.65, 194328, 96483, 97845, 90);
INSERT INTO sor_dim_county VALUES (12065, 12, 2, 'Oberhavel', 1808.17, 211249, 104111, 107138, 117);
INSERT INTO sor_dim_county VALUES (12066, 12, 2, 'Oberspreewald-Lausitz', 1223.46, 110476, 54307, 56169, 90);
INSERT INTO sor_dim_county VALUES (12067, 12, 2, 'Oder-Spree', 2256.75, 178658, 87681, 90977, 79);
INSERT INTO sor_dim_county VALUES (12068, 12, 2, 'Ostprignitz-Ruppin', 2526.55, 99078, 49331, 49747, 39);
INSERT INTO sor_dim_county VALUES (12069, 12, 2, 'Potsdam-Mittelmark', 2592.06, 214664, 106258, 108406, 83);
INSERT INTO sor_dim_county VALUES (12070, 12, 2, 'Prignitz', 2138.58, 76508, 37977, 38531, 36);
INSERT INTO sor_dim_county VALUES (12071, 12, 2, 'Spree-Neiße', 1656.98, 114429, 56269, 58160, 69);
INSERT INTO sor_dim_county VALUES (12072, 12, 2, 'Teltow-Fläming', 2104.22, 168296, 83842, 84454, 80);
INSERT INTO sor_dim_county VALUES (12073, 12, 2, 'Uckermark', 3077.01, 119552, 58670, 60882, 39);
INSERT INTO sor_dim_county VALUES (13003, 13, 0, 'Rostock', 181.36, 208886, 102911, 105975, 1152);
INSERT INTO sor_dim_county VALUES (13004, 13, 0, 'Schwerin', 130.52, 95818, 46054, 49764, 734);
INSERT INTO sor_dim_county VALUES (13071, 13, 2, 'Mecklenburgische Seenplatte', 5495.6, 259130, 127108, 132022, 47);
INSERT INTO sor_dim_county VALUES (13072, 13, 2, 'Landkreis Rostock', 3431.29, 215113, 107111, 108002, 63);
INSERT INTO sor_dim_county VALUES (13073, 13, 2, 'Vorpommern-Rügen', 3215.41, 224684, 110477, 114207, 70);
INSERT INTO sor_dim_county VALUES (13074, 13, 2, 'Nordwestmecklenburg', 2127.08, 156729, 77899, 78830, 74);
INSERT INTO sor_dim_county VALUES (13075, 13, 2, 'Vorpommern-Greifswald', 3945.56, 236697, 115881, 120816, 60);
INSERT INTO sor_dim_county VALUES (13076, 13, 2, 'Ludwigslust-Parchim', 4766.79, 212618, 106198, 106420, 45);
INSERT INTO sor_dim_county VALUES (14511, 14, 0, 'Chemnitz, Stadt', 221.05, 247237, 122248, 124989, 1118);
INSERT INTO sor_dim_county VALUES (14521, 14, 2, 'Erzgebirgskreis', 1827.91, 337696, 165218, 172478, 185);
INSERT INTO sor_dim_county VALUES (14522, 14, 2, 'Mittelsachsen', 2116.85, 306185, 152310, 153875, 145);
INSERT INTO sor_dim_county VALUES (14523, 14, 2, 'Vogtlandkreis', 1412.42, 227796, 111055, 116741, 161);
INSERT INTO sor_dim_county VALUES (14524, 14, 2, 'Zwickau', 949.78, 317531, 154978, 162553, 334);
INSERT INTO sor_dim_county VALUES (14612, 14, 0, 'Dresden, Stadt', 328.48, 554649, 276729, 277920, 1689);
INSERT INTO sor_dim_county VALUES (14625, 14, 2, 'Bautzen', 2395.61, 300880, 148502, 152378, 126);
INSERT INTO sor_dim_county VALUES (14626, 14, 2, 'Görlitz', 2111.41, 254894, 124905, 129989, 121);
INSERT INTO sor_dim_county VALUES (14627, 14, 2, 'Meißen', 1454.59, 242165, 119304, 122861, 166);
INSERT INTO sor_dim_county VALUES (14628, 14, 2, 'Sächsische Schweiz-Osterzgebirge', 1654.19, 245611, 121219, 124392, 148);
INSERT INTO sor_dim_county VALUES (14713, 14, 0, 'Leipzig, Stadt', 297.8, 587857, 288553, 299304, 1974);
INSERT INTO sor_dim_county VALUES (14729, 14, 2, 'Leipzig', 1651.3, 257763, 126380, 131383, 156);
INSERT INTO sor_dim_county VALUES (14730, 14, 2, 'Nordsachsen', 2028.56, 197673, 98218, 99455, 97);
INSERT INTO sor_dim_county VALUES (15001, 15, 0, 'Dessau-Roßlau, Stadt', 244.71, 81237, 39457, 41780, 332);
INSERT INTO sor_dim_county VALUES (15002, 15, 0, 'Halle (Saale), Stadt', 135.03, 239257, 115713, 123544, 1772);
INSERT INTO sor_dim_county VALUES (15003, 15, 0, 'Magdeburg, Landeshauptstadt', 201.01, 238697, 117828, 120869, 1187);
INSERT INTO sor_dim_county VALUES (15081, 15, 2, 'Altmarkkreis Salzwedel', 2293.36, 83765, 41748, 42017, 37);
INSERT INTO sor_dim_county VALUES (15082, 15, 2, 'Anhalt-Bitterfeld', 1453.82, 159854, 78326, 81528, 110);
INSERT INTO sor_dim_county VALUES (15083, 15, 2, 'Börde', 2366.91, 171734, 85660, 86074, 73);
INSERT INTO sor_dim_county VALUES (15084, 15, 2, 'Burgenlandkreis', 1413.75, 180190, 89353, 90837, 127);
INSERT INTO sor_dim_county VALUES (15085, 15, 2, 'Harz', 2104.72, 214446, 105101, 109345, 102);
INSERT INTO sor_dim_county VALUES (15086, 15, 2, 'Jerichower Land', 1576.94, 89928, 44781, 45147, 57);
INSERT INTO sor_dim_county VALUES (15087, 15, 2, 'Mansfeld-Südharz', 1448.92, 136249, 66992, 69257, 94);
INSERT INTO sor_dim_county VALUES (15088, 15, 2, 'Saalekreis', 1433.91, 184582, 91361, 93221, 129);
INSERT INTO sor_dim_county VALUES (15089, 15, 2, 'Salzlandkreis', 1427.33, 190560, 93148, 97412, 134);
INSERT INTO sor_dim_county VALUES (15090, 15, 2, 'Stendal', 2423.42, 111982, 55278, 56704, 46);
INSERT INTO sor_dim_county VALUES (15091, 15, 2, 'Wittenberg', 1930.48, 125840, 61933, 63907, 65);
INSERT INTO sor_dim_county VALUES (16051, 16, 0, 'Erfurt, Stadt', 269.91, 213699, 103920, 109779, 792);
INSERT INTO sor_dim_county VALUES (16052, 16, 0, 'Gera, Stadt', 152.18, 94152, 45517, 48635, 619);
INSERT INTO sor_dim_county VALUES (16053, 16, 0, 'Jena, Stadt', 114.77, 111407, 55665, 55742, 971);
INSERT INTO sor_dim_county VALUES (16054, 16, 0, 'Suhl, Stadt', 103.03, 34835, 16960, 17875, 338);
INSERT INTO sor_dim_county VALUES (16055, 16, 0, 'Weimar, Stadt', 84.48, 65090, 31655, 33435, 770);
INSERT INTO sor_dim_county VALUES (16056, 16, 0, 'Eisenach, Stadt', 104.17, 42370, 20874, 21496, 407);
INSERT INTO sor_dim_county VALUES (16061, 16, 2, 'Eichsfeld', 943.07, 100380, 50364, 50016, 106);
INSERT INTO sor_dim_county VALUES (16062, 16, 2, 'Nordhausen', 713.9, 83822, 41477, 42345, 117);
INSERT INTO sor_dim_county VALUES (16063, 16, 2, 'Wartburgkreis', 1307.44, 123025, 61525, 61500, 94);
INSERT INTO sor_dim_county VALUES (16064, 16, 2, 'Unstrut-Hainich-Kreis', 979.69, 102912, 51119, 51793, 105);
INSERT INTO sor_dim_county VALUES (16065, 16, 2, 'Kyffhäuserkreis', 1037.91, 75009, 37274, 37735, 72);
INSERT INTO sor_dim_county VALUES (16066, 16, 2, 'Schmalkalden-Meiningen', 1210.73, 122347, 61186, 61161, 101);
INSERT INTO sor_dim_county VALUES (16067, 16, 2, 'Gotha', 936.08, 135452, 67468, 67984, 145);
INSERT INTO sor_dim_county VALUES (16068, 16, 2, 'Sömmerda', 806.86, 69655, 34762, 34893, 86);
INSERT INTO sor_dim_county VALUES (16069, 16, 2, 'Hildburghausen', 938.42, 63553, 31766, 31787, 68);
INSERT INTO sor_dim_county VALUES (16070, 16, 2, 'Ilm-Kreis', 843.71, 108742, 54633, 54109, 129);
INSERT INTO sor_dim_county VALUES (16071, 16, 2, 'Weimarer Land', 804.48, 81947, 40645, 41302, 102);
INSERT INTO sor_dim_county VALUES (16072, 16, 2, 'Sonneberg', 433.61, 56196, 27673, 28523, 130);
INSERT INTO sor_dim_county VALUES (16073, 16, 2, 'Saalfeld-Rudolstadt', 1036.03, 106356, 52388, 53968, 103);
INSERT INTO sor_dim_county VALUES (16074, 16, 2, 'Saale-Holzland-Kreis', 815.24, 83051, 41360, 41691, 102);
INSERT INTO sor_dim_county VALUES (16075, 16, 2, 'Saale-Orla-Kreis', 1151.3, 80868, 40119, 40749, 70);
INSERT INTO sor_dim_county VALUES (16076, 16, 2, 'Greiz', 845.98, 98159, 48326, 49833, 116);
INSERT INTO sor_dim_county VALUES (16077, 16, 2, 'Altenburger Land', 569.4, 90118, 44138, 45980, 158);


CREATE TABLE SOR_DIM_GENDER (
	ID smallint,
	NAME varchar(16),
	PRIMARY KEY(ID)
);
INSERT INTO SOR_DIM_GENDER VALUES (0, 'UNKNOWN');
INSERT INTO SOR_DIM_GENDER VALUES (1, 'FEMALE');
INSERT INTO SOR_DIM_GENDER VALUES (2, 'MALE');

CREATE TABLE SOR_DIM_AGEGROUP (
	ID smallint,
	MIN_AGE smallint,
	MAX_AGE smallint,
	PRIMARY KEY(ID)	
);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (0, NULL, NULL);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (1, 0, 4);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (2, 5, 14);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (3, 15, 34);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (4, 35, 59);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (5, 60, 79);
insert into SOR_DIM_AGEGROUP (ID, MIN_AGE, MAX_AGE) values (6, 80, NULL);

CREATE TABLE SOR_FACT_CASE (
	REPORT_DATE date,
	COUNTY_ID smallint,
	GENDER_ID smallint,
	AGEGROUP_ID smallint,
	ONSET_DATE date,
	CASE_COUNT smallint NOT NULL,
	DECEASED_COUNT smallint NOT NULL,
	RECOVERED_COUNT smallint NOT NULL,
	INSERT_DATE timestamp,
	PRIMARY KEY(REPORT_DATE, COUNTY_ID, GENDER_ID, AGEGROUP_ID, ONSET_DATE),
	FOREIGN KEY(GENDER_ID) REFERENCES SOR_DIM_GENDER(ID),
	FOREIGN KEY(AGEGROUP_ID) REFERENCES SOR_DIM_AGEGROUP(ID),
	FOREIGN KEY(COUNTY_ID) REFERENCES SOR_DIM_COUNTY(ID)
);

CREATE TABLE SOR_ARCH_CASE (
	REPORT_DATE date,
	COUNTY_ID smallint,
	GENDER_ID smallint,
	AGEGROUP_ID smallint,
	ONSET_DATE date,
	CASE_COUNT smallint,
	DECEASED_COUNT smallint,
	RECOVERED_COUNT smallint,
	INSERT_DATE timestamp
);
