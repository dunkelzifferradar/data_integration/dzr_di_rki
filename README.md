# dzr_di_rki: Data Integration RKI

Data integration for the Covid-19 reported case data of the German Robert-Koch Institute.


## Setup
### Environment variables
Set the following variables (e.g. via dotenv):
* DB_USER
* DB_PASSWORD
* DB_HOST
* DB_PORT
* DB_NAME
* DB_SSL_CERT (can be empty)
* DB_SSL_KEY (can be empty)