import unittest
import pandas
from dotenv import load_dotenv

import di.integrate_data as cut
from dzr_shared_util.db.db_conn import DBConnector

from consts import ROOT_DIR

load_dotenv(ROOT_DIR + "/.env")


class TestIntegrateData(unittest.TestCase):

    def setUp(self):
        self.conn = DBConnector()

    def test_transform_stg_sor(self):
        """
        Test method that transforms dataframe from staging format to SOR format.
        Assert that resulting dataframe has the expected columns and no null values (which, if they existed, could
        be a sign of bad transformation).
        """
        df_stg = pandas.read_csv(ROOT_DIR + "/test/di/data/STG_RKI.csv", sep=',')
        df_joined = cut._transform_stg_sor(df_stg)
        self.assertEqual(len(df_joined.index), len(df_stg.index))
        expected_cols = cut.SOR_KEY_COLS + [cut.SOR_COL_CASE_COUNT, cut.SOR_COL_DEATH_COUNT,
                                            cut.SOR_COL_RECOVERED_COUNT] + ["INSERT_DATE"]
        self.assertEqual(list(df_joined.columns), expected_cols)
        self.assertFalse(df_joined.isnull().values.any())

    def test_add_hash_cols(self):
        """
        Test hashing method. Assert that new columns with hash values of the provided key columns are added, and that
        these contain no Null values and have the same length (number of rows) than the table before.
        """
        df = pandas.read_csv(ROOT_DIR + "/test/di/data/SOR_RKI_OLD.csv")
        old_cols = list(df.columns)
        old_row_count = len(df.index)
        new_hash_cols = ['TEST_HASH_COL1', 'TEST_HASH_COL2']
        cut._add_hash_cols(df, [['AGEGROUP_ID', 'GENDER_ID'], ['ONSET_DATE', 'RECOVERED_COUNT']],
                           new_hash_cols)
        self.assertEqual(set(df.columns) , set(old_cols + new_hash_cols))
        self.assertEqual(len(df.index), old_row_count)
        self.assertFalse(df[new_hash_cols].isnull().values.any())

    def test_determine_db_action(self):
        """
        Test the function that determines what parts of the dataframe to be written to the SOR case table needs to be
        inserted, updated, deleted.
        Test files have a difference of 2 updates, 1 insert, 1 delete. Assert that the function outcome equals those,
        plus that output dataframes do not contain null values and have same dimensions.
        """
        df_old = pandas.read_csv(ROOT_DIR + "/test/di/data/SOR_RKI_OLD.csv")
        df_new = pandas.read_csv(ROOT_DIR + "/test/di/data/SOR_RKI_NEW.csv")
        all_cols_to_compare = cut.SOR_CASE_COL_NAMES.copy()
        all_cols_to_compare.remove(cut.SOR_COL_INSERTDATE)
        df_inserts, df_updates, df_archives, df_deletes = cut._determine_db_actions(df_new, df_old, cut.SOR_KEY_COLS,
                                                                                    all_cols_to_compare)
        self.assertEqual(len(df_inserts.index), 1)
        self.assertEqual(len(df_updates.index), 2)
        self.assertEqual(len(df_deletes.index), 1)
        self.assertEqual(len(df_archives.index), 3)
        for df in [df_inserts, df_updates, df_deletes, df_archives]:
            self.assertFalse(df.isnull().values.any())
            self.assertEqual(list(df.columns), list(df_old.columns))

    def tearDown(self):
        self.conn.close_connection()


if __name__ == '__main__':
    unittest.main()
