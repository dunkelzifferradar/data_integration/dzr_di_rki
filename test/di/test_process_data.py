import unittest
from unittest.mock import patch

from dotenv import load_dotenv
from pandas import read_csv, DataFrame

import di.process_data as cut
from consts import STG_TABLE_RKI, SORTED_OUT_TABLE_RKI
from dzr_shared_util.db.db_conn import DBConnector

from consts import ROOT_DIR

load_dotenv(ROOT_DIR + "/.env")

dfs_tables_to_write = []


def mock_write_data(df: DataFrame, target_table_name, if_data_exists_in_table='replace', clear_before_load=False):
    dfs_tables_to_write.append((df, target_table_name))


def mock_get_data(tbl_name, where=""):
    samples_df = read_csv(ROOT_DIR + '/test/di/data/' + tbl_name + '.csv', sep=',')
    return samples_df


def mock_init_df_schema(tbl_name):
    schema_df = read_csv(ROOT_DIR + '/test/di/data/' + tbl_name + '.csv', sep=',')
    return schema_df


class TestProcessData(unittest.TestCase):

    def setUp(self):
        self.conn = DBConnector()

    @patch('dzr_shared_util.db.db_conn.DBConnector.init_df_schema', side_effect=mock_init_df_schema)
    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    @patch('dzr_shared_util.db.db_conn.DBConnector.write_data', side_effect=mock_write_data)
    def test_process_data(self, mock_init_df_schema_function, mock_get_data_function, mock_write_data_function):
        """
        The sample raw data file (under data/) was manually edited to contain four invalid entries. For example, an
        invalid IDBUNDESLAND was entered. Therefore, this test asserts that six rows are valid and four are invalid.
        :param mock_init_df_schema_function:
        :param mock_get_data_function:
        :param mock_write_data_function:
        :return:
        """
        cut._process_data()
        self.assertEqual(2, len(dfs_tables_to_write))
        df_tbl_valid = dfs_tables_to_write[0]
        df_tbl_invalid = dfs_tables_to_write[1]
        self.assertEqual(df_tbl_valid[1], STG_TABLE_RKI)
        self.assertEqual(df_tbl_invalid[1], SORTED_OUT_TABLE_RKI)
        self.assertEqual(len(df_tbl_valid[0].index), 6)
        self.assertEqual(len(df_tbl_invalid[0].index), 4)

    def tearDown(self):
        self.conn.close_connection()


if __name__ == '__main__':
    unittest.main()
