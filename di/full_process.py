import logging

from di.load_raw_data import load_raw_data
from di.process_data import process_data
from di.integrate_data import integrate_data

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def full_etl():
    resp, state = load_raw_data()
    if state != 200:
        return resp, state
    resp, state = process_data()
    if state != 200:
        return resp, state
    resp, state = integrate_data()
    if state != 200:
        return resp, state
    return {}, 200
