import datetime
import logging

from arcgis.features import Table
from dzr_shared_util.db.db_conn import DBConnector

from consts import RAW_TABLE_RKI

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

SOURCE_RAW_DATA_RKI = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_COVID19/FeatureServer/0"

conn: DBConnector


def load_raw_data():
    global conn
    try:
        conn = DBConnector()
    except Exception as e:
        logger.debug("DB Connection intialisation failed.")
        logger.exception(e)
        return {"error": "Database connection could not be established"}, 400
    try:
        df_raw_data = __load_rki_data()
    except Exception as e:
        logger.debug("External RKI data could not be pulled.")
        logger.exception(e)
        return {"error": "RKI data could not be loaded"}, 400
    try:
        __store_rki_data_in_db(df_raw_data)
    except Exception as e:
        logger.debug("RKI data could not be written into the database.")
        logger.exception(e)
        return {"error": "RKI data could not be written into database"}, 400
    finally:
        conn.close_connection()
    return {}, 200


class ExternalDataAccessException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


def __load_rki_data():
    """
    Requests the external data from ArcGis.

    :return: pandas.DataFrame: Retrieved data
    """
    try:
        table = Table(SOURCE_RAW_DATA_RKI)
    except Exception as e:
        raise ExternalDataAccessException(e)
    return table.query(as_df=True)


def __store_rki_data_in_db(df_raw_data):
    df_raw_data['INSERT_DATE'] = str(datetime.datetime.now())
    conn.write_data(df_raw_data, RAW_TABLE_RKI, clear_before_load=True)
