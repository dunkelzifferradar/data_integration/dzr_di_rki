import hashlib
import logging

from dzr_shared_util.db.db_conn import DBConnector
from pandas import DataFrame, Series

from consts import STG_TABLE_RKI, SOR_TABLE_CASE, SOR_TABLE_CASE_ARCHIVE

HASH_KEY_COLS = 'KEY_COLS_HASH'
HASH_ALL_COLS = 'ALL_COLS_HASH'
# STG columns
STG_COL_INSERTDATE = 'INSERT_DATE'
STG_COL_NEWCASE = 'NEWCASE_ID'
STG_COL_NEWDEATH = 'NEWDEATH_ID'
STG_COL_NEWRECOV = 'NEWRECOV_ID'
STG_COL_COUNTY = 'COUNTY_ID'
STG_COL_GENDER = 'GENDER_ID'
STG_COL_AGEGROUP = 'AGE_GROUP_ID'
STG_COL_REPORT_DATE = 'REPORT_DATE'
STG_COL_ONSET = 'ONSET_DATE'
STG_COL_CASE_COUNT = 'CASE_COUNT'
STG_COL_RECOV_COUNT = 'RECOV_COUNT'
STG_COL_DEATH_COUNT = 'DEATH_COUNT'
# SOR columns
SOR_COL_ONSET = 'ONSET_DATE'
SOR_COL_GENDER = 'GENDER_ID'
SOR_COL_AGEGROUP = 'AGEGROUP_ID'
SOR_COL_RECOVERED_COUNT = 'RECOVERED_COUNT'
SOR_COL_DEATH_COUNT = 'DECEASED_COUNT'
SOR_COL_CASE_COUNT = 'CASE_COUNT'
SOR_COL_COUNTY = 'COUNTY_ID'
SOR_COL_REPORT_DATE = 'REPORT_DATE'
SOR_COL_INSERTDATE = 'INSERT_DATE'

STG_RKI_COL_NAMES = {'INSERT_DATE', 'STATE_ID', STG_COL_COUNTY, STG_COL_AGEGROUP, STG_COL_GENDER,
                     STG_COL_REPORT_DATE, STG_COL_ONSET, STG_COL_NEWCASE, STG_COL_NEWDEATH, STG_COL_NEWRECOV,
                     STG_COL_CASE_COUNT, STG_COL_DEATH_COUNT, STG_COL_RECOV_COUNT}

SOR_CASE_COL_NAMES = {SOR_COL_REPORT_DATE, SOR_COL_COUNTY, SOR_COL_GENDER, SOR_COL_AGEGROUP, SOR_COL_ONSET,
                      SOR_COL_CASE_COUNT, SOR_COL_DEATH_COUNT, SOR_COL_RECOVERED_COUNT, 'INSERT_DATE'}

SOR_KEY_COLS = [SOR_COL_COUNTY, SOR_COL_AGEGROUP, SOR_COL_GENDER, SOR_COL_REPORT_DATE, SOR_COL_ONSET]

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def __assert_cols(df, col_names):
    assert df is not None, "DataFrame must not be None"
    assert set(df.columns) == col_names, "Dataframe columns do not match expected columns: " + str(
        df.columns) + " vs. " + str(col_names)


def __stg_sor_map_county(stg_county_col: Series):
    """Map external source's county ids to our master data county ids.
    Generally trivial, only special case currently is for Berlin (11000)"""
    return stg_county_col.apply(lambda county_id: 11000 if 11000 <= county_id <= 11999 else county_id)


def __stg_sor_map_gender(stg_gender_col: Series):
    """Trivial, currently"""
    return stg_gender_col.copy()


def __stg_sor_map_agegroup(stg_agegroup_col: Series):
    """Maps age group ids to SOR schema."""
    stg_sor_agegroup_mapping = {
        0: 0,
        1: 0,
        2: 1,
        3: 2,
        4: 3,
        5: 4,
        6: 5,
        7: 6
    }
    return stg_agegroup_col.map(stg_sor_agegroup_mapping)


def _transform_stg_sor(df_stg: DataFrame):
    __assert_cols(df_stg, STG_RKI_COL_NAMES)
    # map columns into SOR format
    df_mapped = df_stg[
        [STG_COL_REPORT_DATE, STG_COL_ONSET, STG_COL_NEWCASE, STG_COL_NEWDEATH, STG_COL_NEWRECOV,
         STG_COL_CASE_COUNT, STG_COL_DEATH_COUNT, STG_COL_RECOV_COUNT]].copy()
    df_mapped[SOR_COL_COUNTY] = __stg_sor_map_county(df_stg[STG_COL_COUNTY])
    df_mapped[SOR_COL_GENDER] = __stg_sor_map_gender(df_stg[STG_COL_GENDER])
    df_mapped[SOR_COL_AGEGROUP] = __stg_sor_map_agegroup(df_stg[STG_COL_AGEGROUP])
    # rename columns that carry the same values (for now) but under a different name in SOR than in STG
    df_mapped.rename(columns={STG_COL_CASE_COUNT: SOR_COL_CASE_COUNT,
                              STG_COL_DEATH_COUNT: SOR_COL_DEATH_COUNT,
                              STG_COL_RECOV_COUNT: SOR_COL_RECOVERED_COUNT}, inplace=True)
    """Determine case, death and recovered counts per attribute combination"""
    # determine cases per attribute combo
    df_cases = _group_by_sum(df_mapped.loc[df_mapped[STG_COL_NEWCASE].isin([0, 1])], SOR_COL_CASE_COUNT, SOR_KEY_COLS)
    # determine deaths per attribute combo
    df_deaths = _group_by_sum(df_mapped.loc[df_mapped[STG_COL_NEWDEATH].isin([0, 1])], SOR_COL_DEATH_COUNT,
                              SOR_KEY_COLS)
    # determine recoveries per attribute combo
    df_recovs = _group_by_sum(df_mapped.loc[df_mapped[STG_COL_NEWRECOV].isin([0, 1])], SOR_COL_RECOVERED_COUNT,
                              SOR_KEY_COLS)
    # Join those three numbers on attribute combo
    df_joined = df_cases.join([df_deaths, df_recovs], how='left')
    df_joined.reset_index(inplace=True)
    df_out: DataFrame = df_joined.apply(lambda x: x)
    df_out.fillna(0, inplace=True)
    df_out[SOR_COL_INSERTDATE] = df_stg[STG_COL_INSERTDATE].max()
    for col in [SOR_COL_CASE_COUNT, SOR_COL_DEATH_COUNT, SOR_COL_RECOVERED_COUNT]:
        df_out[col] = df_out[col].astype(int)
    return df_out


def _group_by_sum(df, sum_col, groupby_attr_cols) -> DataFrame:
    assert sum_col in df.columns and [col in df.columns for col in groupby_attr_cols], \
        "Columns to do group by on must all be in dataframe"
    df_copy = df.copy()
    df_gpby = df_copy[groupby_attr_cols + [sum_col]].groupby(groupby_attr_cols)[[sum_col]].sum()
    # convert group by object back into a regular dataframe
    df_gpby.reset_index(inplace=True)
    df_out: DataFrame = df_gpby.apply(lambda x: x)
    # make sure it's integer
    df_out[sum_col] = df_out[sum_col].astype(int)
    return df_out.set_index(groupby_attr_cols)


def _compute_hash_key(row):
    concat_str = " ".join([str(elem) for elem in row])
    return hashlib.md5(concat_str.encode()).hexdigest()


def _add_hash_cols(df, col_sets: list, new_hash_col_names: list):
    """
    Add new columns with hashes of concatenated (old) columns values
    :param df: the dataframe to add the columns to
    :param col_sets: a list of column sets. For each column set, a hash will be computed and a hash column will be added
    :param new_hash_col_names: a list of column names for the new hash columns.
    :return:
    """
    assert len(col_sets) == len(
        new_hash_col_names), "Number of column sets given must match number of new column names given"
    for (col_set, hash_col) in zip(col_sets, new_hash_col_names):
        df[hash_col] = df[col_set].apply(_compute_hash_key, axis=1)


def _determine_db_actions(df_new, df_old, key_cols, all_cols_to_match):
    """
    Determine entries from new dataframe that need to be inserted (because they are not in the old dataframe), entries
    from the old dataframe that need to be updated (because they exist also in the new dataframe) and entries from the
    old dataframe that must be deleted (because they do not exist in the new dataframe anymore) and archived (because
    they will be overwritten by the new entries).

    If an entry in both dataframes is identical, it is not added to any output dataframe.

    :param df_new: the new dataframe
    :param df_old: the old dataframe
    :param key_cols: the columns to find matching entries on
    :param all_cols_to_match: all columns to match on at all - used to exclude entries from the "update" part when old
    and new are identical, not only on the key columns but on the value columns as well.
    :return: dataframes with entries to be 1) inserted, 2) updated, 3) archived, 4) deleted
    """
    df_old = df_old.copy()
    df_new = df_new.copy()
    _add_hash_cols(df_old, [all_cols_to_match, key_cols], [HASH_ALL_COLS, HASH_KEY_COLS])
    _add_hash_cols(df_new, [all_cols_to_match, key_cols], [HASH_ALL_COLS, HASH_KEY_COLS])
    # determine entries to be inserted
    df_inserts = df_new.loc[~df_new[HASH_KEY_COLS].isin(df_old[HASH_KEY_COLS])]
    # determine entries to be deleted
    df_deletes = df_old.loc[~df_old[HASH_KEY_COLS].isin(df_new[HASH_KEY_COLS])]
    # determine entries to be updated
    df_existing_entries = df_new.loc[df_new[HASH_KEY_COLS].isin(df_old[HASH_KEY_COLS])]
    # throw entries out from update set that are completely identical
    df_updates = df_existing_entries.loc[~df_existing_entries[HASH_ALL_COLS].isin(df_old[HASH_ALL_COLS])]
    # determine entries to be archived - add deletes to it as well
    df_archive_updates: DataFrame = df_old.loc[df_old[HASH_KEY_COLS].isin(df_updates[HASH_KEY_COLS])]
    df_archive = df_archive_updates.append(df_deletes.copy())
    return drop_hash_cols(df_inserts), \
           drop_hash_cols(df_updates), \
           drop_hash_cols(df_archive), \
           drop_hash_cols(df_deletes)


def drop_hash_cols(df):
    return df.drop([HASH_KEY_COLS, HASH_ALL_COLS], axis=1)


def integrate_data():
    global conn
    try:
        conn = DBConnector()
    except Exception as e:
        logger.debug("DB Connection intialisation failed.")
        logger.exception(e)
        return {"error": "Database connection could not be established"}, 400
    try:
        """
        Get Staging data
        """
        try:
            df_stg = conn.get_data(STG_TABLE_RKI)
        except Exception as e:
            logger.debug("Staging data could not be read from the database.")
            raise e
        """
        Perform data transformation into SOR format
        """
        try:
            df_new_sor_case = _transform_stg_sor(df_stg)
        except Exception as e:
            logger.debug("Data transformation from staging to sor failed.")
            raise e
        """
        Determine
            a) which new entries will be inserted
            b) which entries in SOR will be updated (and which old entries must be archived)
            c) which entries do not exist anymore in the new data and need to be archived
            d) which already exist equally in the db and need no action
        """
        try:
            df_old_sor_case = conn.get_data(SOR_TABLE_CASE)
            for col in [SOR_COL_CASE_COUNT, SOR_COL_DEATH_COUNT, SOR_COL_RECOVERED_COUNT]:
                df_old_sor_case[col] = df_old_sor_case[col].astype(int)
        except Exception as e:
            logger.debug("Reading existing SOR data from database failed.")
            raise e
        try:
            all_cols_to_compare = SOR_CASE_COL_NAMES.copy()
            all_cols_to_compare.remove(SOR_COL_INSERTDATE)
            df_inserts, df_updates, df_archives, df_deletes = \
                _determine_db_actions(df_new_sor_case, df_old_sor_case, SOR_KEY_COLS, all_cols_to_compare)
        except Exception as e:
            logger.debug("Splitting the prepared entries into insert, update, delete and archive sets failed")
            raise e
        """
        Insert new data into SOR table
        """
        try:
            conn.write_data(df_inserts, SOR_TABLE_CASE,
                            clear_before_load=False, commit=False)
        except Exception as e:
            logger.debug("SOR data could not be written into the database.")
            raise e
        """
        Insert entries to be archived into archive table
        """
        try:
            conn.write_data(df_archives, SOR_TABLE_CASE_ARCHIVE,
                            clear_before_load=False, commit=False)
            # todo: delete those that have disappeared
        except Exception as e:
            logger.debug("Existing SOR data to be overwritten could not be archived.")
            raise e
        """
        Update existing entries in SOR with new values
        """
        try:
            conn.update_data(df_updates, SOR_TABLE_CASE,
                             update_keys=SOR_KEY_COLS,
                             update_vals=[SOR_COL_INSERTDATE, SOR_COL_CASE_COUNT, SOR_COL_DEATH_COUNT, SOR_COL_RECOVERED_COUNT]
                             , commit=False)
        except Exception as e:
            logger.debug("Existing SOR data could not be updated.")
            raise e
        """
        Delete entries from SOR table that do not exist in the new data any more. They were already archived above as
        the delete dataframe is part of the archive dataframe
        """
        try:
            conn.delete_data(SOR_TABLE_CASE, where=df_deletes, commit=False)
        except Exception as e:
            logger.debug("Existing SOR data that does not exist in new data anymore could not be deleted.")
            raise e
        conn.commit()
    except Exception as e:
        fail_msg = "Integration of processed data failed. Actions will be rolled back"
        logger.error(fail_msg)
        logger.exception(e)
        try:
            conn.rollback()
        except Exception as e:
            logger.error("Rollback failed")
            logger.exception(e)
        return {"error": fail_msg}, 400
    finally:
        conn.close_connection()
    return {}, 200
