import logging

from dzr_shared_util.db.db_conn import DBConnector, DBConnectionException
from pandas import DataFrame

from consts import RAW_TABLE_RKI, STG_TABLE_RKI, SORTED_OUT_TABLE_RKI, DIM_TABLE_COUNTY, DIM_TABLE_STATE, \
    DIM_TABLE_AGEGROUP, DIM_TABLE_NEWCASE, DIM_TABLE_NEWDEATH, DIM_TABLE_NEWRECOV, DIM_TABLE_GENDER, NULL_DATE_VALUE

RAW_START_OF_ILLNESS_BOOLEAN_COL_NAME = 'ISTERKRANKUNGSBEGINN'

RAW_REF_DATE_COL_NAME = 'REFDATUM'

RAW_GENDER_COL_NAME = 'GESCHLECHT'

RAW_AGE_GROUP_COL_NAME = 'ALTERSGRUPPE'

DIM_GENDER_NAME_COL_NAME = 'NAME'

DIM_TABLE_ID_COL_NAME = 'ID'

DIM_AGE_GROUP_DESCRIPTION_COL_NAME = 'DESCR'

SORTED_OUT_REASON_COL_NAME = 'SO_REASON'

dq_check_params = [
    (DIM_TABLE_STATE, 'IDBUNDESLAND', 'ID'),
    (DIM_TABLE_COUNTY, 'IDLANDKREIS', 'ID'),
    (DIM_TABLE_AGEGROUP, 'ALTERSGRUPPE', 'DESCR'),
    (DIM_TABLE_GENDER, 'GESCHLECHT', 'NAME'),
    (DIM_TABLE_NEWCASE, 'NEUERFALL', 'ID'),
    (DIM_TABLE_NEWDEATH, 'NEUERTODESFALL', 'ID'),
    (DIM_TABLE_NEWRECOV, 'NEUGENESEN', 'ID')
]

raw_stg_simple_col_mapping = {
    'INSERT_DATE': 'INSERT_DATE',
    'IDBUNDESLAND': 'STATE_ID',
    'IDLANDKREIS': 'COUNTY_ID',
    'NEUERFALL': 'NEWCASE_ID',
    'NEUERTODESFALL': 'NEWDEATH_ID',
    'NEUGENESEN': 'NEWRECOV_ID',
    'ANZAHLFALL': 'CASE_COUNT',
    'ANZAHLTODESFALL': 'DEATH_COUNT',
    'ANZAHLGENESEN': 'RECOV_COUNT',
    'MELDEDATUM': 'REPORT_DATE'
}
# dimension tables
dfs_dim_tbl = {}
# logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
# db
conn: DBConnector


def __dq_and_transform_data(df_raw):
    """
    Transforms the raw input data and runs data quality (dq) checks using values from the database's dimension (DIM)
    tables.
    Sorts out those entries that do not pass the dq checks.
    :param df_raw:
    :return: Tuple of
        0) a dataframe with transformed and valid entries ready to be put into the database
        1) a dataframe made up of sorted out entries.
    """
    if df_raw is None:
        raise ValueError("Dataframe to transform must not be None")
    # init result dfs
    df_res = DataFrame()
    df_sortedout = conn.init_df_schema(SORTED_OUT_TABLE_RKI)
    """Data Quality checks"""
    df_valid = df_raw
    for dq_check_param in dq_check_params:
        assert len(dq_check_param) == 3
        # df of valid entries gets updated each round, based on the valid entries
        df_valid, df_invalids = __dq_check(df_valid, dq_check_param[0], dq_check_param[1], dq_check_param[2])
        if all(col in df_sortedout for col in df_invalids) and len(df_sortedout.columns) == len(df_invalids.columns):
            df_sortedout = df_sortedout.append(df_invalids)
        else:
            raise ValueError("Columns of dataframes must be equivalent to be appended. "
                             "Instead: '" + str(df_sortedout.columns) + "' vs. '" + str(df_invalids.columns) + "'")
    """Transformation into target (staging table) schema"""
    # 1: simple renames
    for old_col_name in raw_stg_simple_col_mapping.keys():
        res_col_name = raw_stg_simple_col_mapping[old_col_name]
        __assert_col_in_df(old_col_name, df_valid)
        df_res[res_col_name] = df_valid[old_col_name]
    # 2: complex conversions
    #   Age Group
    df_dim_agegroup = dfs_dim_tbl[DIM_TABLE_AGEGROUP]
    __assert_col_in_df(DIM_AGE_GROUP_DESCRIPTION_COL_NAME, df_dim_agegroup)
    __assert_col_in_df(DIM_TABLE_ID_COL_NAME, df_dim_agegroup)
    dict_agegroup = dict(
        zip(df_dim_agegroup[DIM_AGE_GROUP_DESCRIPTION_COL_NAME], df_dim_agegroup[DIM_TABLE_ID_COL_NAME]))
    __assert_col_in_df(RAW_AGE_GROUP_COL_NAME, df_valid)
    df_res['AGE_GROUP_ID'] = df_valid[RAW_AGE_GROUP_COL_NAME].map(dict_agegroup)
    #   Gender
    df_dim_gender = dfs_dim_tbl[DIM_TABLE_GENDER]
    __assert_col_in_df(DIM_GENDER_NAME_COL_NAME, df_dim_gender)
    dict_gender = dict(zip(df_dim_gender[DIM_GENDER_NAME_COL_NAME], df_dim_gender[DIM_TABLE_ID_COL_NAME]))
    __assert_col_in_df(RAW_GENDER_COL_NAME, df_valid)
    df_res['GENDER_ID'] = df_valid[RAW_GENDER_COL_NAME].map(dict_gender)
    #   Onset date
    __assert_col_in_df(RAW_REF_DATE_COL_NAME, df_valid)
    __assert_col_in_df(RAW_START_OF_ILLNESS_BOOLEAN_COL_NAME, df_valid)
    df_res['ONSET_DATE'] = df_valid[[RAW_REF_DATE_COL_NAME, RAW_START_OF_ILLNESS_BOOLEAN_COL_NAME]].apply(
        __determine_onset_date, axis=1)
    return df_res, df_sortedout


def __assert_col_in_df(col_name, df):
    if col_name not in df.columns:
        ValueError("Column " + col_name + " is not present in dataframe. "
                                          "Instead, it has columns: " + str(df.columns))


def __determine_onset_date(row):
    """
    In the source data, the field ISTERKRANKUNGSBEGINN is set to 1 if the REFDATUM denotes the date of the start of
    the illness. If it is set to 0, it means that the start of the illness is unknown and simply the MELDEDATUM is used.
    Instead of None, this function set a Null/None representative date value. This prevents problems upon sql insert.
    :param row:
    :return:
    """
    assert row is not None and len(row) == 2
    if row[1] == 1:
        return row[0]
    else:
        return NULL_DATE_VALUE


def __dq_check(df_current_valids, dim_table_name, raw_col_name, dim_col_name):
    """
    Check the raw data's column against the valid values in the dimension table and return the valid and invalid rows
    separately in dataframes.
    :param df_current_valids:
    :param dim_table_name:
    :param raw_col_name:
    :param dim_col_name:
    :return:
    """
    assert df_current_valids is not None
    assert dim_table_name is not None
    assert raw_col_name is not None
    assert dim_col_name is not None
    df_dim_tbl = conn.get_data(dim_table_name)
    dfs_dim_tbl[dim_table_name] = df_dim_tbl
    if raw_col_name not in df_current_valids.columns:
        raise ValueError("Column " + raw_col_name + " not present in dataframe. "
                                                    "Instead, it has columns: '" + str(df_current_valids.columns) + "'")
    if dim_col_name not in df_dim_tbl.columns:
        raise ValueError("Column " + dim_col_name + " not present in dataframe for table " + dim_table_name)
    df_valid = df_current_valids[df_current_valids[raw_col_name].isin(df_dim_tbl[dim_col_name])].copy()
    df_invalid = df_current_valids[~df_current_valids[raw_col_name].isin(df_dim_tbl[dim_col_name])].copy()
    if SORTED_OUT_REASON_COL_NAME in df_invalid.columns:
        raise ValueError("Column " + SORTED_OUT_REASON_COL_NAME + " already exists in dataframe. "
                                                                  "Should only be added here")
    df_invalid[SORTED_OUT_REASON_COL_NAME] = raw_col_name
    return df_valid, df_invalid


def process_data():
    try:
        _process_data()
    except DBConnectionException as dbce:
        return {"error": "Database connection could not be established"}, 400
    except Exception as e:
        return {"error": "Processing RKI data failed"}, 400
    return {}, 200


def _process_data():
    global conn
    try:
        conn = DBConnector()
    except Exception as e:
        logger.debug("DB Connection initialisation failed. Error was: ")
        logger.exception(e)
        raise DBConnectionException from e
    try:
        try:
            df_raw = conn.get_data(RAW_TABLE_RKI)
        except Exception as e:
            logger.debug("Raw data could not be read from the database.")
            raise e
        try:
            df_stg_rki, df_sorted_out = __dq_and_transform_data(df_raw)
        except Exception as e:
            logger.debug("Data Quality checks and data transformation failed.")
            raise e
        try:
            conn.write_data(df_stg_rki, target_table_name=STG_TABLE_RKI, clear_before_load=True)
        except Exception as e:
            logger.debug("Staging data could not be written into the database.")
            raise e
        try:
            conn.write_data(df_sorted_out, target_table_name=SORTED_OUT_TABLE_RKI, clear_before_load=False)
        except Exception as e:
            logger.debug("Entries sorted out through data quality checks could not be written into database.")
            raise e
    finally:
        conn.close_connection()
