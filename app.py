""" Module containing basic connexion backend. """

import os

from di.load_raw_data import load_raw_data
from di.process_data import process_data
from di.integrate_data import integrate_data
from di.full_process import full_etl
import connexion
import sentry_sdk
from dotenv import load_dotenv
from flask_cors import CORS

load_dotenv(".env")

SENTRY_HANDLE = os.getenv("SENTRY_HANDLE")
APP_VERSION = os.getenv("APP_VERSION")

sentry_sdk.init(
    SENTRY_HANDLE,
    release=f"dzr-di-rki@{APP_VERSION}",
    traces_sample_rate=1.0
)

app = connexion.App(
    __name__, options={"swagger_ui": True} # os.environ.get("SWAGGER_UI", False)}
)
CORS(app.app)
app.add_api("app.yml")
application = app.app

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, server="gevent")
